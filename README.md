# Destiny

# 框架介绍
## Destiny，一个简单的后台管理系统 :smirk: 

 **前端采用TDesign-Vue-Next,后端为Spring Boot3.1.0，目前只适配了MySQL** 

## 软件架构
整体为前后端分离架构

- Java后端：Spring Boot+JDK17，MyBatis-flex，Hutool，MySQL

- Vue前端：Vue3，Vite，TDesign-Vue-Next
    
## 安装教程

### 前端

- [前端地址](https://gitee.com/T1meMach1ne/AdminUI)

- 安装Node.js 14.0.0以上版本

- 根目录运行`npm install pnpm -g --registry=https://registry.npmmirror.com`以切换镜像

- 根目录运行`pnpm install`以安装依赖

- 根目录运行`pnpm dev`启动项目
    
### Java后端

- 打开application-local中配置数据库信息，运行AdminApplication类即可启动

## 使用说明

### Java后端
- [Spring Boot](https://spring.io/projects/spring-boot/)
- [Java工具类库](https://hutool.cn/docs/#/)
- [MyBatis-flex](https://mybatis-flex.com/)

### Vue前端
- [Vue3](https://v3.cn.vuejs.org/)
- [TDesign-Vue-Next](https://tdesign.tencent.com/vue-next/overview/)

## 项目交流

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
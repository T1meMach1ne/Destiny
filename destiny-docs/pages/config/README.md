---
title: 配置
index: false
icon: config
category:
  - 配置向导
---

## 目录

### 基础配置
- [Docker命令](/config/basics/docker.md)
- [Jenkins命令](/config/basics/jenkins.md)
- [linux命令](/config/basics/linux.md)
- [linux服务自启](/config/basics/linux-autoservice.md)
- [npm命令](/config/basics/npm.md)

### 部署文档
- [安装包](/config/deploy/install.md)
- [docker-java配置](/config/deploy/docker-java.md)
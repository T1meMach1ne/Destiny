---
title:  docker配置jenkins
category: 配置向导
tag:
  - 容器配置
---

# Docker配置SpringBoot项目
## Jenkins的安装及配置
### 为docker设置镜像地址，在/etc/docker下新建daemon.json内容为
```bash
vi /etc/docker
{"registry-mirrors": ["https://rgiropog.mirror.aliyuncs.com"]}
```

### 下载Jenkins的Docker镜像
```bash
docker pull jenkins/jenkins:lts
```

### 在Docker容器中运行Jenkins
```bash
docker run -p 8080:8080 -p 50000:5000 --name jenkins \
-u root \
-v /mydata/jenkins_home:/var/jenkins_home \
-d jenkins/jenkins:lts
```

### 查看密码
```bash
docker logs jenkins
```
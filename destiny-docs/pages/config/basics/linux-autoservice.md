---
title:  linux服务自启
category: 配置向导
tag:
  - 服务器配置
---

# Centos设置自启服务
## 1. 增加服务文件
### 进入服务文件目录，创建服务文件
```bash
cd /etc/systemd/system/
vi pmwise.service
```

## 2. 编辑内容
```bash
[Unit]
Description=pmwise    
After=syslog.target network.target
[Service]
Type=simple
ExecStart=/java/jdk1.8.0_261/bin/java -jar /www/wwwroot/pmwise-admin/xxx.jar --spring.config.location=/www/wwwroot/pmwise-admin
ExecStop=/bin/kill -15 $MAINPID
[Install]
WantedBy=multi-user.target
```

## 3. 操作服务
### 启动服务
```bash
systemctl start pmwise
systemctl start pmwise.service
```

### 停止服务
```bash
systemctl stop pmwise
systemctl stop pmwise.service
```

### 重启配置
```bash
systemctl daemon-reload
```

### 开机启动
```bash
systemctl enable pmwise
systemctl enable pmwise.service
```
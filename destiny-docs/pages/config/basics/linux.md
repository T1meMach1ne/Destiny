---
title:  linux
category: 配置向导
tag:
  - 服务器配置
---

# Linux常用命令
## 系统服务管理systemctl
systemctl命令是service和chkconfig命令的组合体，可用于管理系统。
### 输出系统中各个服务的状态
```bash
systemctl list-units --type=service
```

### 查看服务的运行状态
```bash
systemctl status firewalld
```

### 关闭服务
```bash
systemctl stop firewalld
```

### 启动服务
```bash
systemctl start firewalld
```

### 重新启动服务（不管当前服务是启动还是关闭）
```bash
systemctl restart firewalld
```

### 重新载入配置信息而不中断服务
```bash
systemctl reload firewalld
```

### 禁止服务开机自启动
```bash
systemctl disable firewalld
```

### 设置服务开机自启动
```bash
systemctl enable firewalld
```

## 查看日志journalctl
### 以flow形式查看日志
```bash
journalctl -f
```

### 查看指定服务日志
```bash
journalctl -u docker.serivce
```

### 查看指定日期日志
```bash
journalctl --since="2018-09-21 10:21:00" -u dockerjournalctl --since="2018-09-21 10:21:00" --until="2018-09-21 10:22:00" -u docker
```

### 查看指定当天指定日期之后的日志
```bash
journalctl --since 12:10:00 _SYSTEMD_UNIT="pmwise.service"
```
## 查看系统进程ps
### 显示系统进程运行动态
```bash
ps -ef
```

### 查看sshd进程的运行动态
```bash
ps -ef |grep sshd
```

## 解压缩tar
### 将/etc文件夹中的文件归档到文件etc.tar（并不会进行压缩）
```bash
tar -cvf /mydata/etc.tar /etc
```

### 用gzip压缩文件夹/etc中的文件到文件etc.tar.gz
```bash
tar -zcvf /mydata/etc.tar.gz /etc
```

### 用bzip2压缩文件夹/etc到文件/etc.tar.bz2
```bash
tar -jcvf /mydata/etc.tar.bz2 /etc
```

### 分页查看压缩包中内容（gzip）
```bash
tar -ztvf /mydata/etc.tar.gz |more -c -10
```

### 解压文件到当前目录（gzip）
```bash
tar -zxvf /mydata/etc.tar.gz
```

### 解压文件到指定目录（gzip）
```bash
tar -zxvf /mydata/etc.tar.gz -C /mydata/etc
```

## 网络信息netstat
### 显示当前网络接口状态
```bash
ifconfig
```

### 查看当前路由信息
```bash
netstat -rn
```

### 查看所有有效TCP连接
```bash
netstat -an
```

### 查看系统中启动的监听服务
```bash
netstat -tulnp
```

### 查看处于连接状态的系统资源信息
```bash
netstat -atunp
```

## Yum是Yellow dog Updater, Modified的缩写，能够在线自动下载RPM包并安装，可以自动处理依赖性关系，并且一次安装所有依赖的软件包，非常方便！
### 安装软件包
```bash
yum install nginx
```

### 检查可以更新的软件包
```bash
yum check-update
```

### 更新指定的软件包
```bash
yum update nginx
```

### 在资源库中查找软件包信息
```bash
yum info nginx*
```

### 列出已经安装的所有软件包
```bash
yum info installed
```

### 列出软件包名称
```bash
yum list nginx*
```

### 模糊搜索软件包
```bash
yum search nginx
```

### 用于拷贝文件，例如将test1目录复制到test2目录
```bash
cp -r /mydata/tes1 /mydata/test2
```

## 防火墙Firewall
### 开启防火墙
```bash
systemctl start firewalld
```

### 关闭防火墙
```bash
systemctl start firewalld
```

### 查看防火墙状态
```bash
systemctl start firewalld
```

### 设置开机启动
```bash
firewall-cmd --reload
```

### 禁用开机启动
```bash
systemctl disable firewalld
```

### 重启防火墙
```bash
firewall-cmd --reload
```

### 开放端单个端口（修改后需要重启防火墙方可生效）
```bash
firewall-cmd --zone=public --add-port=8080/tcp --permanent
```

### 开放端多个端口（修改后需要重启防火墙方可生效）
```bash
firewall-cmd --zone=public --add-port=20000-29999/tcp --permanent
```

### 查看开放的端口
```bash
firewall-cmd --list-ports
```

### 关闭端口
```bash
firewall-cmd --zone=public --remove-port=8080/tcp --permanent
```
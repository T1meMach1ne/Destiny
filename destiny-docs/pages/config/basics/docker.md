---
title:  docker
category: 配置向导
tag:
  - 容器配置
---

# Docker 简介
`Docker` 是一个开源的应用容器引擎，让开发者可以打包应用及依赖包到一个可移植的镜像中，然后发布到任何流行的Linux或Windows机器上。使用Docker可以更方便地打包、测试以及部署应用程序。

Docker通常用于如下场景：
- web应用的自动化打包和发布；
- 自动化测试和持续集成、发布；
- 在服务型环境中部署和调整数据库或其他的后台应用；
- 从头编译或者扩展现有的OpenShift或Cloud Foundry平台来搭建自己的PaaS环境。

## Docker常用命令
### 安装yum-utils
```bash
yum install -y yum-utils device-mapper-persistent-data lvm2
```

### 为yum源添加docker仓库位置
```bash
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
```

### 安装docker服务
```bash
yum install docker-ce
```

### 启动docker服务
```bash
systemctl start docker
```

## Docker镜像常用命令
### 搜索镜像
```bash
docker search java
```

### 下载镜像
```bash
docker pull java:8
```
### 列出镜像
```bash
docker images
```

### 指定名称删除镜像
```bash
docker rmi java:8
```

### 打包镜像
-t 表示指定镜像仓库名称/镜像名称:镜像标签 .表示使用当前目录下的Dockerfile文件 docker 
```bash
build -t mall/xxxx:1.0-SNAPSHOT .
```

### 推送镜像 登录Docker Hub docker login # 给本地镜像打标签为远程仓库名称 docker tag mall/:1.0-SNAPSHOT macrodocker/xxxx:1.0-SNAPSHOT # 推送到远程仓库
```bash
docker push macrodocker/xxxx:1.0-SNAPSHOT
```

## Docker容器常用命令
### 新建并启动容器
```bash
docker run -p 80:80 --name nginx \ 
-e TZ="Asia/Shanghai"\ 
-v /mydata/nginx/html:/usr/share/nginx/html \ 
-d nginx:1.17.0
```

- -p：将宿主机和容器端口进行映射，格式为：宿主机端口:容器端口；
- --name：指定容器名称，之后可以通过容器名称来操作容器；
- -e：设置容器的环境变量，这里设置的是时区；
- -v：将宿主机上的文件挂载到宿主机上，格式为：宿主机文件目录:容器文件目录；
- -d：表示容器以后台方式运行。

### 列出运行中的容器
```bash
docker ps
```

### 列出所有容器
```bash
docker ps -a
```

### 停止容器
注意：
ContainerName表示容器名称，ContainerId-表示容器ID，可以使用容器名称的命令，基本也支持使用容器ID，比如下面的停止容器命令。
```bash
docker stop $ContainerName(or $ContainerId)
```

### 强制停止容器
```bash
docker kill $ContainerName
```

### 启动容器
```bash
docker start $ContainerName
```

### 进入容器
#### 先查询出容器的pid
```bash
docker inspect --format "{{.State.Pid}}"$ContainerName
```

#### 根据容器的pid进入容器
```bash
nsenter --target "$pid" --mount --uts --ipc --net --pid
```

### 删除容器
#### 删除指定容器
```bash
docker rm $ContainerName
```
#### 按名称通配符删除容器，比如删除以名称mall-开头的容器
```bash
docker rm`docker ps -a |grep mall-* |awk'{print $1}'`
```

#### 强制删除所有容器
```bash
docker rm -f $(docker ps -a -q)
```

### 查看容器的日志
#### 查看容器产生的全部日志
```bash
docker logs $ContainerName
```

#### 动态查看容器产生的日志
```bash
docker logs -f $ContainerName
```

#### 查看容器的IP地址
```bash
docker inspect --format '{{ .NetworkSettings.IPAddress }}'$ContainerName
```

### 修改容器的启动方式
#### 将容器启动方式改为always 
```bash
docker container update --restart=always $ContainerName
```

### 同步宿主机时间到容器
```bash
docker cp /etc/localtime $ContainerName:/etc/
```

### 执行容器内部命令
```bash
docker exec -it $ContainerName /bin/bash
```

### 指定账号进入容器内部
#### 使用root账号进入容器内部
```bash
docker exec -it --user root $ContainerName /bin/bash
```

#### 查看容器的IP地址
```bash
docker inspect --format '{{ .NetworkSettings.IPAddress }}'$ContainerName
```

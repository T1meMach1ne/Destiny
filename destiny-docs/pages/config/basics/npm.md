---
title:  npm
category: 配置向导
tag:
  - 前端命令
---

# 前端常用命令
## 查看 npm 版本
```bash
npm -v
```
## npm淘宝镜像（可选）
```bash
# 设置全局的npm淘宝镜像
npm config set registry https://registry.npm.taobao.org
# 也可以切换回默认全局镜像
npm config set registry https://registry.npmjs.org
```
## 安装模块
::: tabs 

@tab pnpm
```bash
pnpm i                       # 安装所有依赖
pnpm i --offline             # 仅从应用商店脱机安装
pnpm i --frozen-lockfile	   # pnpm-lock.yaml不更新
pnpm i --lockfile-only       # 仅更新pnpm-lock.yaml
```

@tab npm
```bash
npm init           # npm 初始化当前目录
npm i              # 安装所有依赖
npm i pkg          # 安装模块到默认dependencies
npm i pkg -g       # 安装到配置的全局目录下
npm i pkg -S       # 安装到dependencies生产依赖
npm i pkg -D       # 安装到devDependencies开发依赖
npm i pkg@version  # 安装指定版本依赖
```

:::

## 添加模块
::: tabs 

@tab pnpm
```bash
pnpm add pkg       # 安装模块到默认dependencies
pnpm add -g pkg    # 安装到配置的全局目录下
pnpm add pkg       # 安装到dependencies生产依赖
pnpm add -D pkg    # 安装到devDependencies开发依赖
pnpm add pkg@next  # 安装标记为 next 的版本
pnpm add pkg@3.0.0 # 安装指定版本依赖
```

@tab npm
:::

## 卸载模块
::: tabs 
@tab pnpm
```bash
pnpm rm pkg             # 卸载模块，但不卸载模块留在package.json中的对应信息
pnpm rm pkg --global    # 卸载全局模块
pnpm rm pkg -D          # 仅删除 devDependencies 中列出的依赖包
pnpm rm pkg -O          # 仅删除 optionalDependencies 中列出的依赖包
pnpm rm pkg -P          # 仅删除 dependencies 中列出的依赖包
```
@tab npm
```bash
npm uninstall pkg         # 卸载模块，但不卸载模块留在package.json中的对应信息
npm uninstall pkg -g      # 卸载全局模块
npm uninstall pkg --save  # 卸载模块，同时卸载留在package.json中dependencies下的信息
npm uninstall pkg --D     # 卸载模块，同时卸载留在package.json中devDependencies下的信息
```
:::

## 更新模块
::: tabs
@tab pnpm 
```bash
pnpm up            # 更新所有依赖项，并遵循package.json
pnpm up --latest   # 更新所有依赖项，忽略package.json
pnpm up pgk@2      # 对 v2 上最新版本的更新pkg
pnpm up "@babel/*" # 更新作用域下的所有依赖项@babel
```

@tab npm 
```bash
npm update pkg          # 更新最新版本
npm update pkg@2.1.0    # 更新到指定版本号
npm install pkg@latest  # 更新到最后一个新版本
```
:::
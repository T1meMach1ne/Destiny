---
title: docker配置相关
category: 配置向导
tag:
  - 容器配置
---

# Docker 配置 SpringBoot 项目

## 为 docker 设置镜像地址，在/etc/docker 下新建 daemon.json 内容为

```bash
vi /etc/docker
{"registry-mirrors": ["https://rgiropog.mirror.aliyuncs.com"]}
```

## MySQL 安装

### 下载 MySQL5.7 的 docker 镜像

```bash
docker pull mysql:5.7
```

### 启动 MySQL5.7 服务，当找不到 config 配置文件时，使用版本 5.7-2;8.0 版本使用 8.0.21-1

::: tabs

@tab 版本 5.7-1

```bash
docker run -p 3306:3306 --name mysql --privileged=true \
-e MYSQL_ROOT_PASSWORD=123456.zxc  \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/conf:/etc/mysql \
-d mysql
```

@tab 版本 5.7-2

```bash
docker run \-p 3306:3306 --name mysql \
-e MYSQL_ROOT_PASSWORD=123456.zxc \
-v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/log:/var/log/mysql \
-v /mydata/mysql/config/my.cnf:/etc/mysql/my.cnf \
--name mysql \
-d mysql:8.0.21
```

@tab 版本 8.0.21-1

```bash
docker run -it -d --name mysql --net=host \
-m 500m -v /mydata/mysql/data:/var/lib/mysql \
-v /mydata/mysql/config:/etc/mysql/conf.d  \
-e MYSQL_ROOT_PASSWORD=123456.zxc \
-e TZ=Asia/Shanghai mysql:8.0.21 \
--lower_case_table_names=1
```

:::

参数说明

- -p 3306:3306：设置容器的 3306 端口映射到主机的 3306 端口
- --name mysql：设置容器的名称为 mysql
- -v /mydata/mysql/conf:/etc/mysql：将配置文件夹挂在到主机
- -v /mydata/mysql/log:/var/log/mysql：将日志文件夹挂载到主机
- -v /mydata/mysql/data:/var/lib/mysql/：将数据文件夹挂载到主机
- -e MYSQL_ROOT_PASSWORD=root：初始化 root 用户的密码

### 进入运行 MySQL 的 docker 容器

```bash
docker exec -it mysql /bin/bash
```

### 使用 MySQL 命令打开客户端

```bash
mysql -uroot -p123456.zxc --default-character-set=utf8;
```

### 修改 root 权限，使得任何 ip 都能访问

```bash
grant all privileges on *.* to 'root' @'%' identified by'123456.zxc';
flush privileges;
```

### 开启 3306 端口的防火墙允许外部访问，并刷新防火墙

```bash
firewall-cmd --zone=public --add-port=3306/tcp --permanent
firewall-cmd --reload
```

## Redis 安装

### 下载 Redis5.0 的 docker 镜像

```bash
docker pull redis:5
```

### 启动 Redis 服务

```bash
docker run -p 6379:6379 --name redis \
-v /mydata/redis/data:/data \
-d redis:5 redis-server --requirepass 123456.zxc
```

## Nginx 安装

### 下载 Nginx1.10 的 docker 镜像

docker pull nginx:1.20

### 先运行一次容器（为了拷贝配置文件）

```bash
docker run -p 80:80 --name nginx \
-v /mydata/nginx/html:/usr/share/nginx/html \
-v /mydata/nginx/logs:/var/log/nginx \
-d nginx:1.20
```

### 将容器内的配置文件拷贝到指定目录

```bash
docker container cp nginx:/etc/nginx /mydata/nginx/
```

### 修改拷出来的文件名称

```bash
cd /mydata/nginx mv nginx conf
```

### 终止并删除容器

```bash
docker stop nginx
docker rm nginx
```

### 使用如下命令启动 Nginx 服务

```bash
docker run -p 80:80 --name nginx \
-v /mydata/nginx/html:/usr/share/nginx/html \
-v /mydata/nginx/logs:/var/log/nginx  \
-v /mydata/nginx/conf:/etc/nginx \
-d nginx:1.20
```

## Jenkins 的安装及配置

### 下载 Jenkins 的 Docker 镜像

```bash
docker pull jenkins/jenkins:lts
```

### 在 Docker 容器中运行 Jenkins

```bash
docker run -p 8080:8080 -p 50000:5000 --name jenkins \
-u root \
-v /mydata/jenkins_home:/var/jenkins_home \
-d jenkins/jenkins:lts
```

### 查看密码

```bash
docker logs jenkins
```

## RabbitMQ 安装

### 下载 rabbitmq3.7.15 的 docker 镜像

```bash
docker pull rabbitmq:3.7.15
```

### 使用如下命令启动 RabbitMQ 服务

```bash
docker run -p5672:5672 -p 15672:15672 --name rabbitmq \
-d rabbitmq:3.7.15
```

### 进入容器并开启管理功能

```bash
docker exec -it rabbitmq /bin/bash
rabbitmq-plugins enable rabbitmq_management
```

### 开启防火墙

```bash
firewall-cmd --zone=public --add-port=15672/tcp --permanentfirewall-cmd --reload
```

## Minio 安装

### 下载 minio 的 docker 镜像

```bash
docker pull minio/minio
```

### 创建挂载目录

```bash
mkdir -p /mydata/minio/config
mkdir -p /mydata/minio/data
```

### 启动 minio 服务

```bash
docker run --name minio \
-p 9090:9090 \
-d --restart=always \
-e "MINIO_ROOT_USER=minioAdmin" \
-e "MINIO_ROOT_PASSWORD=minioAdmin" \
-v /mydata/minio/data:/data \
-v /mydata/minio/config:/root/.minio \
minio/minio server /data \--console-address ":9090" --address ":9000"
```

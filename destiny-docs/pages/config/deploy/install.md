---
title:  安装包
category: 配置向导
tag:
  - 安装包
---

# 安装包
## 本地汇总云盘下载
[阿里云网盘下载地址](https://www.aliyundrive.com/s/XskgCQa7x45)  
提取码: `kc66`   
点击链接保存，或者复制本段内容，打开「阿里云盘」APP ，无需下载极速在线查看，视频原画倍速播放。

## 在线下载
### WindTerm
WindTerm 是一款支持 Windows、Linux 和 macOS的SSH客户端工具  
下载地址：  
- github [WindTerm](https://github.com/kingToolbox/WindTerm/releases)

### WinSCP
WinSCP 是一款好用SSH图形化SFTP客户端工具  
下载地址：  
- 官网 [WinSCP](https://winscp.net/eng/docs/lang:chs)

### AnotherRedisDesktopManager
Another Redis DeskTop Manager作为一款基于nodejs开发的Redis可视化管理工具，可以运行在Windows、Linux、Mac平台   
下载地址：  
- github [WindTerm](https://github.com/qishibo/AnotherRedisDesktopManager/releases)

### mRemoteNG
mRemoteNG 是一款远程桌面管理工具  
下载地址：  
- 官网 [mRemoteNG](https://mremoteng.org/download)

### PDManer
PDManer 是一个款由国人开发的开源跨平台数据库建模工具，支持windows,mac,linux等操作系统  
下载地址：  
- Gitee [PDManer](https://gitee.com/robergroup/pdmaner/releases)

### ToDesk
ToDesk 作为一款安全免费不限速的远程控制软件，支持跨平台的远程控制，有且不限于PC对PC，iOS/Android也可以直接控制  
下载地址：  
- 官网 [ToDesk](https://www.todesk.com/download.html)


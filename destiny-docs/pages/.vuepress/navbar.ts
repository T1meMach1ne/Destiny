import { navbar } from 'vuepress-theme-hope';

export default navbar([
	'/home',
	{ text: '使用指南', icon: 'creative', link: '/guide/' },
	{ text: '配置向导', icon: 'config', link: '/config/' },
	{ text: '开发规范', icon: 'standard', link: '/standard/' },
	{ text: '其他信息', icon: 'others', link: '/others/' },
]);

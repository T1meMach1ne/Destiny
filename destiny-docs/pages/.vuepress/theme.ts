import { hopeTheme } from "vuepress-theme-hope";
import navbar from "./navbar";
import sidebar from "./sidebar";

export default hopeTheme({
  hostname: "",
  
  author: {
    name: "T1meMach1ne",
    url: "",
  },

  iconAssets: "iconfont",

  logo: "/logo.svg",

  repo: "",

  docsDir: "demo/src",

  // navbar
  navbar: navbar,

  // sidebar
  sidebar: sidebar,

  footer: "",

  displayFooter: true,

  pageInfo: ["Author", "Original", "Date", "Category", "Tag", "ReadingTime"],

  encrypt: {
    config: {
      "/guide/encrypt.html": ["1234"],
    },
  },

  plugins: {
    mdEnhance: {
      mermaid: true,
      tabs: true,
      mark: true,
      flowchart: true,
      presentation: {
        plugins: ["highlight", "math", "search", "notes", "zoom"],
      },
      vpre: true,
      codetabs: true,
      align: true,
      demo: {
        codepen: true,
      }
    },
    blog: {
      autoExcerpt: true,
    },
  },
  //#region 个性化配置
  fullscreen: true,
  themeColor: {
    blue: "#2196f3",
    red: "#f26d6d",
    green: "#3eaf7c",
    orange: "#fb9b5f",
  },
  backToTop: true,
  //#endregion
});

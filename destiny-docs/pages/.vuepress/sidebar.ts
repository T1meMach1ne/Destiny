import { sidebar } from 'vuepress-theme-hope';

export default sidebar([
	'/home',
	{
		text: '如何使用',
		icon: 'creative',
		prefix: '/guide/',
		link: '/guide/',
		children: 'structure',
	},
	{
		text: '配置向导',
		icon: 'config',
		prefix: '/config/',
		link: '/config/',
		children: [
			{
				text: '基础命令',
				icon: 'customize',
				prefix: 'basics/',
				children: [
					{ text: 'Docker命令', icon: 'docker', link: 'docker' },
					{ text: 'Linux命令', icon: 'linux', link: 'linux' },
					{ text: 'Linux服务自启', icon: 'linux', link: 'linux-autoservice' },
					{ text: 'NPM命令', icon: 'npm', link: 'npm' },
					{ text: 'Jenkins命令', icon: 'jenkins', link: 'jenkins' },
				],
			},
			{
				text: '部署文档',
				icon: 'folder',
				prefix: 'deploy/',
				children: [
					{ text: '安装包', icon: 'install', link: 'install' },
					{ text: 'Docker', icon: 'java', link: 'docker-java' },
					// {text: "windows", icon: "windows", link: "windows"},
					// {text: "linux", icon: "linux", link: "linux"},
				],
			},
		],
	},
	{
		text: '开发规范',
		icon: 'config',
		prefix: '/standard/',
		link: '/standard/',
		children: [
			{
				text: '前端规范',
				icon: 'customize',
				prefix: 'frontend/',
				children: [{ text: 'vue3', icon: 'vue', link: 'vue3' }],
			},
			{
				text: '后端文档',
				icon: 'folder',
				prefix: 'backend/',
				children: [{ text: 'java', icon: 'java', link: 'java' }],
			},
		],
	},
]);

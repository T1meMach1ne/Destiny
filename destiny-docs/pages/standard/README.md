---
title: 规范
index: false
icon: config
category:
  - 开发规范
---

## 目录

### 前端规范
- [VUE3](/standard/frontend/vue3.md)

### 后端规范
- [java](/standard/backend/java.md)
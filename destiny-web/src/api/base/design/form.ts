import { request } from '@/utils/request';

export function getList(data: any) {
  return request.get({
    url: '/base/design/form/list',
    method: 'get',
    params: data,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/design/form/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/design/form/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/design/form/delete',
    method: 'post',
    data,
  });
}

import { request } from '@/utils/request';

export function getList(params: any) {
  return request.get({
    url: '/base/data/enumItem/list',
    method: 'get',
    params,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/data/enumItem/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/data/enumItem/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/data/enumItem/delete',
    method: 'post',
    data,
  });
}

export function getEnumItemList(data: any) {
  return request.post({
    url: '/base/data/enumItem/getEnumItemList',
    method: 'post',
    data,
  });
}

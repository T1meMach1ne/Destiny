import { request } from '@/utils/request';

export function getList(params: any) {
  return request.get({
    url: '/base/data/table/list',
    method: 'get',
    params,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/data/table/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/data/table/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/data/table/delete',
    method: 'post',
    data,
  });
}

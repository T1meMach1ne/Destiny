import { request } from '@/utils/request';

export function getList(data: any) {
  return request.get({
    url: '/base/data/dbLink/list',
    method: 'get',
    params: data,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/data/dbLink/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/data/dbLink/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/data/dbLink/delete',
    method: 'post',
    data,
  });
}

export function TestDbConnection(data: any) {
  return request.post({
    url: `/base/data/dbLink/test`,
    method: 'post',
    data,
  });
}

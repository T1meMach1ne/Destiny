import { request } from '@/utils/request';

export function getList(data: any) {
  return request.get({
    url: '/base/data/enum/list',
    method: 'get',
    params: data,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/data/enum/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/data/enum/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/data/enum/delete',
    method: 'post',
    data,
  });
}

import { request } from '@/utils/request';

export function getList(params: any) {
  return request.get({
    url: '/base/data/category/list',
    method: 'get',
    params,
  });
}

export function getTree(params: any) {
  return request.get({
    url: '/base/data/category/tree',
    method: 'get',
    params,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/data/category/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/data/category/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/data/category/delete',
    method: 'post',
    data,
  });
}

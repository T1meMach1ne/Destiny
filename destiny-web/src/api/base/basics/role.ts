import { request } from '@/utils/request';

export function getList(data: any) {
  return request.get({
    url: '/base/basics/role/list',
    method: 'get',
    params: data,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/basics/role/info/${id}`,
    method: 'get',
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/basics/role/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/basics/role/delete',
    method: 'post',
    data,
  });
}

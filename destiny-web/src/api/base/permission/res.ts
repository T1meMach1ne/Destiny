import { request } from '@/utils/request';

export function getRouters() {
  return request.get({
    url: '/base/permission/res/getRouters',
    method: 'get',
  });
}

export function getMenuTree(params: any) {
  return request.get({
    url: '/base/permission/res/getMenuTree',
    method: 'get',
    params,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/base/permission/res/info/${id}`,
    method: 'get',
  });
}

export function getResTree(params: any) {
  return request.get({
    url: '/base/permission/res/getResTree',
    method: 'get',
    params,
  });
}

export function getList(params: any) {
  return request.get({
    url: '/base/permission/res/list',
    method: 'get',
    params,
  });
}

export function doEdit(data: any) {
  return request.post({
    url: '/base/permission/res/save',
    method: 'post',
    data,
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/base/permission/res/delete',
    method: 'post',
    data,
  });
}

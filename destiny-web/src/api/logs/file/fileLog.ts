import { request } from '@/utils/request';

export function getList(params: any) {
  return request.get({
    url: '/logs/basics/log/list',
    method: 'get',
    params,
  });
}

export function getInfo(id: string) {
  return request.get({
    url: `/logs/basics/log/info/${id}`,
    method: 'get',
  });
}

export function doDelete(data: any) {
  return request.post({
    url: '/logs/basics/log/delete',
    method: 'post',
    data,
  });
}

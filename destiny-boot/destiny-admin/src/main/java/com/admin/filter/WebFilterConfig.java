package com.admin.filter;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2021/3/16 9:00
 */
@Configuration
public class WebFilterConfig implements WebMvcConfigurer {

    /**
     * 添加拦截器
     *
     * @param registry 注册表
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册TestInterceptor拦截器
        InterceptorRegistration registration = registry.addInterceptor(new WebInterceptor());
        //不拦截的路径
        registration.excludePathPatterns(excludePath());
    }

    /**
     * 排除路径
     *
     * @return {@link List}<{@link String}>
     */
    private List<String> excludePath() {
        List<String> list = new ArrayList<>();
        list.add("/api/system/auth/Login");
        list.add("/v3/api-docs/**");
        list.add("/webjars/**");
        list.add("/swagger/**");
        list.add("/swagger-resources/**");
        list.add("/swagger-ui.html");
        list.add("/swagger-ui/**");
        list.add("/doc.html");
        list.add("/login");
        return list;
    }
}

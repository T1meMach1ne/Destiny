package com.admin.filter;

import cn.dev33.satoken.stp.StpUtil;
import com.admin.enums.system.ResultCode;
import com.admin.model.Result;
import com.admin.util.JsonUtil;
import com.admin.util.ServletUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2021/3/16 8:58
 */
public class WebInterceptor implements HandlerInterceptor {

    /**
     * 在请求处理之前进行调用（Controller方法调用之前）
     *
     * @param request  请求
     * @param response 响应
     * @param handler  处理程序
     * @return boolean
     */
    @Override
    public boolean preHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler) {
        //sa-token认证校验
        if (!StpUtil.isLogin()) {
            return this.sessionOverdue(response);
        }
        StpUtil.updateLastActiveToNow();
        return true;
    }

    private boolean sessionOverdue(HttpServletResponse response) {
        Result<?> result = Result.fail(ResultCode.SessionOverdue.getCode(), ResultCode.SessionOverdue.getMessage());
        ServletUtil.renderString(response, JsonUtil.getObjectToString(result));
        return false;
    }

    /**
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     *
     * @param request      请求
     * @param response     响应
     * @param handler      处理程序
     * @param modelAndView 模型和视图
     */
    @Override
    public void postHandle(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, ModelAndView modelAndView) {
    }

    /**
     * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
     *
     * @param request  请求
     * @param response 响应
     * @param handler  处理程序
     * @param ex       异常
     */
    @Override
    public void afterCompletion(@NotNull HttpServletRequest request, @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {
    }
}

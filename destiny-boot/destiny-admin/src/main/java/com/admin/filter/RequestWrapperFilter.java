package com.admin.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * 通过过滤器将ServletRequest
 * 封装成ContentCachingRequestWrapper
 * body被读取后，会被它缓存
 *
 * @author yh_liu
 * @version 1.0
 * @since 2023/1/5
 */
@Component
public class RequestWrapperFilter extends OncePerRequestFilter {

    private static final String OPTIONS = "OPTIONS";

    /**
     * 做过滤器内部
     *
     * @param request  请求
     * @param response 响应
     * @param chain    链
     * @throws ServletException servlet异常
     * @throws IOException      ioexception
     */
    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, @NotNull FilterChain chain) throws ServletException, IOException {
        response.addHeader("Access-Control-Allow-Credentials", "true");
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "*");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type,Content-Disposition,Authorization,*");
        response.addHeader("Authorization", "*");
        response.addHeader("Access-Control-Max-Age", "3600");
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition");
        // 如果是OPTIONS则结束请求
        if (OPTIONS.equals(request.getMethod())) {
            response.getWriter().println("ok");
            return;
        }
        chain.doFilter(request, response);
    }
}

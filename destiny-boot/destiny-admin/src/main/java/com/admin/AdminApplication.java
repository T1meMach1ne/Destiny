package com.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * springboot启动类
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022-7-7
 */
@SpringBootApplication
public class AdminApplication {
    /**
     * springBoot入口方法
     *
     * @param args 参数
     */
    public static void main(String[] args) {
        SpringApplication.run(AdminApplication.class, args);
        System.out.println("启动完成");
    }
}

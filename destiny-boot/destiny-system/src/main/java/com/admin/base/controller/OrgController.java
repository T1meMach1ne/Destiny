package com.admin.base.controller;


import com.admin.base.entity.OrgEntity;
import com.admin.base.model.org.OrgTreeVO;
import com.admin.base.service.OrgService;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统组织表 前端控制器
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Tag(name = "org", description = "组织管理")
@RestController
@RequestMapping("/api/base/basics/org")
@RequiredArgsConstructor
public class OrgController {

    private final OrgService orgService;

    /**
     * 查询列表
     *
     * @return 列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public List<OrgEntity> list() {
        return orgService.list();
    }

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/tree")
    public List<OrgTreeVO> tree(PageModel pageModel) {
        return orgService.tree(pageModel);
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询组织信息")
    @GetMapping("/info/{id}")
    public OrgEntity info(@PathVariable String id) {
        return orgService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     */
    @Operation(summary = "保存组织")
    @PostMapping("/save")
    public Boolean save(@RequestBody OrgEntity entity) {
        return orgService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除组织")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return orgService.delete(ids);
    }
}

package com.admin.base.mapper;

import com.admin.base.entity.RoleEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 角色信息表 Mapper 接口
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Mapper
public interface RoleMapper extends BaseMapper<RoleEntity> {

    /**
     * 根据userId获取角色信息
     *
     * @param userId 用户id
     * @return 角色信息
     */
    @Select(" select sr.* from s_b_role sr join s_b_user_role sur on sr.id = sur.role_id where sur.user_id = #{userId}")
    List<RoleEntity> getUserRole(String userId);
}

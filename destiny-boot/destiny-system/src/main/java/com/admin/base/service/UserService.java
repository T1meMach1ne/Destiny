package com.admin.base.service;

import com.admin.base.entity.UserEntity;
import com.admin.base.model.user.ChangePwdInput;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
public interface UserService extends IService<UserEntity> {

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<UserEntity> list(PageModel pageModel);

    /**
     * 根据账号获取当前用户
     *
     * @param userName 用户名
     * @return 当前用户
     */
    UserEntity getInfoByUserName(String userName);

    /**
     * 根据userId获取当前用户
     *
     * @param id 用户id
     * @return 当前用户
     */
    UserEntity getInfoByUserId(String id);

    /**
     * 保存
     *
     * @param entity 用户信息
     * @return 是否成功
     * @throws CustomException 错误信息
     */
    Boolean saveUpdate(UserEntity entity) throws CustomException;

    /**
     * 修改密码
     *
     * @param changePwdInput 修改密码对象
     * @return 是否成功
     */
    Boolean changedPwd(ChangePwdInput changePwdInput);

    /**
     * 解除锁定
     *
     * @param idsModel 用户id数组
     * @return 是否成功
     */
    Boolean unLock(IdsModel idsModel);

    /**
     * 删除
     *
     * @param idsModel 用户id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel idsModel);
}

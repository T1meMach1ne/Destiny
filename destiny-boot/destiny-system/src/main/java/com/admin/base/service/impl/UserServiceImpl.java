package com.admin.base.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.admin.base.entity.UserEntity;
import com.admin.base.mapper.UserMapper;
import com.admin.base.model.user.ChangePwdInput;
import com.admin.base.service.UserService;
import com.admin.constant.DbNameConst;
import com.admin.constant.SymbolConstant;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.enums.system.FieldOrderEnum;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;


/**
 * 用户表;(base_user)表服务实现类
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class UserServiceImpl extends ServiceImpl<UserMapper, UserEntity> implements UserService {

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<UserEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();

        if (StringUtils.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(UserEntity::getUserName, pageModel.getSearchValue()).or(
                    x -> {
                        x.like(UserEntity::getRealName, pageModel.getSearchValue());
                    }
            );
        }
        //排序
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(UserEntity::getCreateTime).desc();
        } else {
            queryWrapper = FieldOrderEnum.ASC.getKey().equalsIgnoreCase(pageModel.getSortOrder()) ? queryWrapper.orderBy(pageModel.getSortField(), Boolean.TRUE) : queryWrapper.orderBy(pageModel.getSortField(), Boolean.FALSE);
        }
        Page<UserEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<UserEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 根据账号获取当前用户
     *
     * @param userName 用户名
     * @return 当前用户
     */
    @Override
    public UserEntity getInfoByUserName(String userName) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(UserEntity::getUserName, userName);
        return this.getOne(queryWrapper);
    }

    /**
     * 根据id获取当前用户
     *
     * @param id 用户id
     * @return 当前用户
     */
    @Override
    public UserEntity getInfoByUserId(String id) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(UserEntity::getId, id);
        return this.getOne(queryWrapper);
    }

    /**
     * 保存
     *
     * @param entity 用户信息
     * @return 是否成功
     */
    @Override
    public Boolean saveUpdate(UserEntity entity) throws CustomException {
        if (checkExist(entity)) {
            throw new CustomException(ErrorCodesEnum.A0111);
        } else {
            if (ObjectUtil.isEmpty(entity.getId())) {
                entity.setId(GuIdUtil.nextId());
            }
            return this.saveOrUpdate(entity);
        }
    }

    /**
     * 验证用户名是否存在
     *
     * @param entity 用户信息
     * @return 是否存在
     */
    public Boolean checkExist(UserEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(UserEntity::getUserName, entity.getUserName());
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            queryWrapper.ne(UserEntity::getId, entity.getId());
            List<UserEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        } else {
            List<UserEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }


    /**
     * 修改密码
     *
     * @param changePwdInput 修改密码对象
     * @return 是否成功
     */
    @Override
    public Boolean changedPwd(ChangePwdInput changePwdInput) {
        List<UserEntity> list = this.getListByIds(changePwdInput.getIds());
        list.forEach(x -> x.setPassword(DigestUtil.md5Hex(x.getUserName().toLowerCase() + changePwdInput.getPassword())));
        return this.updateBatch(list);
    }

    /**
     * 解除锁定
     *
     * @param model 用户id数组
     * @return 是否成功
     */
    @Override
    public Boolean unLock(IdsModel model) {
        List<UserEntity> list = this.listByIds(model.getIds());
        list.forEach(x -> x.setLockTime(null).setErrorCount(0));
        return this.updateBatch(list);
    }

    /**
     * 删除
     *
     * @param idsModel 用户id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel idsModel) {
        List<UserEntity> list = this.listByIds(idsModel.getIds());
        return this.removeByIds(list.stream().map(UserEntity::getId).toList());
    }

    /**
     * 根据id获取用户列表
     *
     * @param ids 逗号分隔id
     * @return 用户列表
     */
    private List<UserEntity> getListByIds(String ids) {
        List<String> data = Arrays.asList(ids.split(SymbolConstant.STRING_COMMA));
        return this.listByIds(data);
    }
}

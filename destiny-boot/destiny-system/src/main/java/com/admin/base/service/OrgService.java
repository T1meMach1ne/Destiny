package com.admin.base.service;

import com.admin.base.entity.OrgEntity;
import com.admin.base.model.org.OrgTreeVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * 系统组织表 服务类
 *
 * @author yh_liu
 * @since 2022-07-21
 */
public interface OrgService extends IService<OrgEntity> {

    /**
     * /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<OrgTreeVO> tree(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    OrgEntity info(String id);

    /**
     * 保存
     *
     * @param entity 组织信息
     * @return 是否成功
     */
    boolean saveUpdate(OrgEntity entity);

    /**
     * 删除
     *
     * @param ids 组织id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);
}

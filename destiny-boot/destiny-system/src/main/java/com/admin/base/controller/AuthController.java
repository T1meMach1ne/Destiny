package com.admin.base.controller;

import cn.dev33.satoken.stp.StpUtil;
import cn.hutool.core.convert.Convert;
import com.admin.base.entity.UserEntity;
import com.admin.base.model.auth.LoginInput;
import com.admin.base.model.auth.LoginOutVO;
import com.admin.base.service.AuthService;
import com.admin.model.Result;
import com.admin.model.UserInfo;
import com.admin.util.CurrentUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Tag(name = "auth", description = "登录相关")
@RestController
@RequestMapping("/api/base/basics/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final CurrentUtil currentUtil;

    /**
     * 登录方法
     *
     * @param loginInput 登录参数
     * @return jwt-token
     */
    @Operation(summary = "登录")
    @PostMapping("/login")
    public String login(@RequestBody LoginInput loginInput) {
        UserEntity userEntity = authService.checkLogin(loginInput);
        StpUtil.login(userEntity.getId());
        String token = StpUtil.getTokenValue();
        UserInfo userInfo = new UserInfo();
        authService.userInfo(userInfo, userEntity);
        userInfo.setToken(token);
        //写入redis
        currentUtil.setCurrent(userInfo);
        return token;
    }

    /**
     * 获取用户信息
     *
     * @return 当前登录的用户信息
     */

    @Operation(summary = "获取用户信息")
    @GetMapping("/userInfo")
    public LoginOutVO getUserInfo() {
        UserInfo userInfo = currentUtil.getUserInfo();
        return Convert.convert(LoginOutVO.class, userInfo);
    }

    /**
     * 退出登录
     *
     * @return 当前登录的用户信息
     */
    @Operation(summary = "用户退出登录")
    @GetMapping("/logout")
    public Result<?> logout() {
        //移除登录相关redis数据
        currentUtil.clearUserAuth();
        currentUtil.removeCurrent();
        return Result.success("注销成功");
    }
}

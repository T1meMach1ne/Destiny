package com.admin.base.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.base.entity.RoleEntity;
import com.admin.base.mapper.RoleMapper;
import com.admin.base.service.RoleService;
import com.admin.constant.DbNameConst;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.exception.CustomException;
import com.admin.model.BaseEntity;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 角色信息表 服务实现类
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
public class RoleServiceImpl extends ServiceImpl<RoleMapper, RoleEntity> implements RoleService {
    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<RoleEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(RoleEntity::getName, pageModel.getSearchValue()).or(
                    t -> {
                        t.like(RoleEntity::getCode, pageModel.getSearchValue());
                    }
            );
        }
        Page<RoleEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<RoleEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 查询列表
     *
     * @param userId 用户id
     * @return 列表数据
     */
    @Override
    public List<RoleEntity> getUserRole(String userId) {
        return this.mapper.getUserRole(userId);
    }

    /**
     * 获取信息
     *
     * @param id 主键id
     * @return 当前数据
     */
    @Override
    public RoleEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 保存
     *
     * @param entity 角色信息
     * @return 是否成功
     */
    @Override
    public boolean saveUpdate(RoleEntity entity) throws CustomException {
        if (checkExist(entity)) {
            throw new CustomException(ErrorCodesEnum.DB000.getKey());
        } else {
            if (ObjectUtil.isEmpty(entity.getId())) {
                entity.setId(GuIdUtil.nextId());
            }
            return this.saveOrUpdate(entity);
        }
    }

    /**
     * 验证角色编号是否存在
     *
     * @param entity 角色信息
     * @return 是否存在
     */
    public Boolean checkExist(RoleEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(RoleEntity::getCode, entity.getCode());
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            queryWrapper.ne(RoleEntity::getId, entity.getId());
            List<RoleEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        } else {
            List<RoleEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 删除
     *
     * @param ids id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel ids) {
        List<RoleEntity> list = this.listByIds(ids.getIds());
        return this.removeByIds(list.stream().map(BaseEntity::getId).toList());
    }
}

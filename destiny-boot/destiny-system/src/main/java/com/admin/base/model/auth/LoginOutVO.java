package com.admin.base.model.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Data
public class LoginOutVO {

    /**
     * 主键id
     */
    @Schema(description = "主键id")
    private String userId;

    /**
     * 账号
     */
    @Schema(description = "账号")
    private String userName;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String realName;
}

package com.admin.base.model.org;

import com.admin.model.TreeNode;
import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/24
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class OrgTreeVO extends TreeNode<OrgTreeVO> {

    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    @Column("name")
    private String name;

    /**
     * 简称
     */
    @Schema(description = "简称")
    @Column("short_name")
    private String shortName;

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column("code")
    private String code;

    /**
     * 类型
     */
    @Schema(description = "类型")
    @Column("type")
    private String type;

    /**
     * 状态
     */
    @Schema(description = "状态")
    @Column("state")
    private String state;
}

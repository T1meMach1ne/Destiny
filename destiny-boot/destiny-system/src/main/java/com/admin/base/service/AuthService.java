package com.admin.base.service;

import com.admin.base.entity.UserEntity;
import com.admin.base.model.auth.LoginInput;
import com.admin.model.UserInfo;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
public interface AuthService {

    /**
     * 登录验证
     *
     * @param inputVO 登录参数
     * @return 当前用户
     */
    UserEntity checkLogin(LoginInput inputVO);

    /**
     * 根据userId获取用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    UserEntity getUserInfo(String userId);

    /**
     * 转化用户信息
     *
     * @param userInfo   需要转化的对象
     * @param userEntity 数据库源数据
     */
    void userInfo(UserInfo userInfo, UserEntity userEntity);
}

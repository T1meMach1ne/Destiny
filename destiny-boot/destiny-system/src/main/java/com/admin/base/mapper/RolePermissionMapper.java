package com.admin.base.mapper;

import com.admin.permission.entity.RoleResEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
@Mapper
public interface RolePermissionMapper extends BaseMapper<RoleResEntity> {

}





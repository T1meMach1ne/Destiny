package com.admin.base.service;

import com.admin.base.entity.UserRoleEntity;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * 用户角色关系表;(base_user_role)表服务接口
 *
 * @author hw_ren
 * @version 1.0
 * @since : 2022-7-11
 */
public interface UserRoleService extends IService<UserRoleEntity> {
    /**
     * 给用户绑定角色
     *
     * @param userId     用户id
     * @param roleIdList 角色id集合
     * @return 是否成功
     */
    Boolean bindRole(String userId, List<String> roleIdList);
}
package com.admin.base.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 用户角色关系表;
 *
 * @author hw_ren
 * @version 1.0
 * @since : 2022-7-11
 */
@Data
@Accessors(chain = true)
@Table("base_basics_user_role")
@Schema(name = "用户角色关系")
@EqualsAndHashCode(callSuper = true)
public class UserRoleEntity extends BaseEntity {

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    @Column(value = "role_id")
    private String roleId;

    /**
     * 用户id
     */
    @Schema(description = "用户id")
    @Column(value = "user_id")
    private String userId;
}

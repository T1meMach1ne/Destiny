package com.admin.base.service;

import com.admin.base.entity.RoleEntity;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * 角色信息表 服务类
 *
 * @author yh_liu
 * @since 2022-07-21
 */
public interface RoleService extends IService<RoleEntity> {
    /**
     * 获取用户角色
     *
     * @param userId 用户id
     * @return 角色信息
     */
    List<RoleEntity> getUserRole(String userId);

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<RoleEntity> list(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    RoleEntity info(String id);

    /**
     * 保存
     *
     * @param entity 角色信息
     * @return 是否成功
     * @throws CustomException 错误信息
     */
    boolean saveUpdate(RoleEntity entity) throws CustomException;

    /**
     * 删除
     *
     * @param ids 角色id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);
}

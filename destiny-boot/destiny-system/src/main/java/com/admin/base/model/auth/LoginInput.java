package com.admin.base.model.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Data
@EqualsAndHashCode()
public class LoginInput {

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    private String phone;

    /**
     * 用户名
     */
    @Schema(description = "用户名")
    private String account;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;

    /**
     * 验证码
     */
    @Schema(description = "验证码")
    private String verifyCode;

    /**
     * 记住账号
     */
    @Schema(description = "记住账号")
    private Boolean checked;

    /**
     * 登录设备
     */
    @Schema(description = "登录设备")
    private String device;
}

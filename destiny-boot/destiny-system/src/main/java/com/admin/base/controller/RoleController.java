package com.admin.base.controller;


import com.admin.base.entity.RoleEntity;
import com.admin.base.service.RoleService;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 角色表 前端控制器
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Tag(name = "role", description = "角色管理")
@RestController
@RequestMapping("/api/base/basics/role")
@RequiredArgsConstructor
public class RoleController {

    private final RoleService roleService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<?> list(PageModel pageModel) {
        List<RoleEntity> list = roleService.list(pageModel);
        PageResult<RoleEntity> pageResult = new PageResult<>(list);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询角色信息")
    @GetMapping("/info/{id}")
    public RoleEntity info(@PathVariable String id) {
        return roleService.info(id);
    }


    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存角色")
    @PostMapping("/save")
    public Boolean save(@RequestBody RoleEntity entity) throws CustomException {
        return roleService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除角色")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return roleService.delete(ids);
    }
}

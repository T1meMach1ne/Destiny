package com.admin.base.mapper;

import com.admin.base.entity.OrgEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 系统组织表 Mapper 接口
 * </p>
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Mapper
public interface OrgMapper extends BaseMapper<OrgEntity> {

}

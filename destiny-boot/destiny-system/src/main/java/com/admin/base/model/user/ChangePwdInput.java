package com.admin.base.model.user;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/23
 */
@Data
public class ChangePwdInput {

    /**
     * id集合
     */
    @Schema(description = "id集合")
    private String ids;

    /**
     * 密码
     */
    @Schema(description = "密码")
    private String password;
}

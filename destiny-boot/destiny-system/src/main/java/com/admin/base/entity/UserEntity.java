package com.admin.base.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * 系统用户表
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Data
@Table("base_basics_user")
@Schema(name = "系统用户")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UserEntity extends BaseEntity {

    /**
     * 账号
     */
    @Schema(description = "账号")
    @Column(value = "user_name")
    private String userName;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    @Column(value = "real_name")
    private String realName;

    /**
     * 密码
     */
    @Schema(description = "密码")
    @Column(value = "password")
    private String password;

    /**
     * 性别
     */
    @Schema(description = "性别")
    @Column(value = "sex")
    private String sex;

    /**
     * 生日
     */
    @Schema(description = "生日")
    @Column(value = "birthday")
    private String birthday;

    /**
     * 办公电话
     */
    @Schema(description = "办公电话")
    @Column(value = "office_phone")
    private String officePhone;

    /**
     * 公司id
     */
    @Schema(description = "公司id")
    @Column(value = "company_id")
    private Long companyId;

    /**
     * 公司名称
     */
    @Schema(description = "公司名称")
    @Column(value = "company_name")
    private String companyName;

    /**
     * 手机号
     */
    @Schema(description = "手机号")
    @Column(value = "mobile_phone")
    private String mobilePhone;

    /**
     * 组织id
     */
    @Schema(description = "组织id")
    @Column(value = "org_id")
    private Long orgId;

    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    @Column(value = "org_name")
    private String orgName;

    /**
     * 最后一次登录ip
     */
    @Schema(description = "最后一次登录ip")
    @Column(value = "last_login_ip")
    private String lastLoginIp;

    /**
     * 最后一次登录时间
     */
    @Schema(description = "最后一次登录时间")
    @Column(value = "last_login_time")
    private LocalDateTime lastLoginTime;


    /**
     * 最后一次登录设备
     */
    @Schema(description = "最后一次登录设备")
    @Column(value = "last_login_plat_form")
    private String lastLoginPlatForm;

    /**
     * 锁定时间
     */
    @Schema(description = "锁定时间")
    @Column(value = "lock_time")
    private LocalDateTime lockTime;

    /**
     * 密码错误次数
     */
    @Schema(description = "密码错误次数")
    @Column(value = "error_count")
    private Integer errorCount;

    /**
     * 修改密码时间
     */
    @Schema(description = "修改密码时间")
    @Column(value = "update_pwd_time")
    private LocalDateTime updatePwdTime;

    /**
     * 状态
     */
    @Schema(description = "状态")
    @Column(value = "state")
    private String state;

}

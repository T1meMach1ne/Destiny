package com.admin.base.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.base.entity.OrgEntity;
import com.admin.base.mapper.OrgMapper;
import com.admin.base.model.org.OrgTreeVO;
import com.admin.base.service.OrgService;
import com.admin.constant.DbNameConst;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.admin.util.TreeUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 系统组织表 服务实现类
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
public class OrgServiceImpl extends ServiceImpl<OrgMapper, OrgEntity> implements OrgService {

    /**
     * /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<OrgTreeVO> tree(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderBy(OrgEntity::getSortIndex).asc();
        List<OrgEntity> data = this.list(queryWrapper);
        List<OrgEntity> treeData = data;
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            treeData = data.stream().filter(t -> StrUtil.nullToEmpty(t.getCode()).contains(pageModel.getSearchValue())
                    || StrUtil.nullToEmpty(t.getName()).contains(pageModel.getSearchValue())).collect(Collectors.toList());
        }
        List<OrgTreeVO> modeList = BeanUtil.copyToList(treeData, OrgTreeVO.class);
        return TreeUtil.build(modeList);
    }

    /**
     * 获取信息
     *
     * @param id 查询参数
     * @return 当前数据
     */
    @Override
    public OrgEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 保存
     *
     * @param entity 组织信息
     * @return 是否成功
     */
    @Override
    public boolean saveUpdate(OrgEntity entity) {
        if (ObjectUtil.isEmpty(entity.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        return this.saveOrUpdate(entity);
    }

    /**
     * 删除
     *
     * @param model 组织id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel model) {
        List<OrgEntity> list = this.listByIds(model.getIds());
        return this.removeByIds(list.stream().map(OrgEntity::getId).toList());
    }

}

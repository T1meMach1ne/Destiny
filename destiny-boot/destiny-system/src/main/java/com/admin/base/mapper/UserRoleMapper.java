package com.admin.base.mapper;

import com.admin.base.entity.UserRoleEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 用户角色关系表;(base_user_role)表数据库访问层
 *
 * @author hw_ren
 * @version 1.0
 * @since : 2022-7-11
 */
@Mapper
public interface UserRoleMapper extends BaseMapper<UserRoleEntity> {
}
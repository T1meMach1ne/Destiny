package com.admin.base.controller;

import com.admin.base.entity.UserEntity;
import com.admin.base.model.user.ChangePwdInput;
import com.admin.base.model.user.UserListVO;
import com.admin.base.service.UserService;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import com.admin.util.JsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户管理
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Tag(name = "user", description = "用户管理")
@RestController
@RequestMapping("/api/base/basics/user")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<?> list(PageModel pageModel) {
        List<UserEntity> list = userService.list(pageModel);
        List<UserListVO> result = JsonUtil.getJsonToList(JsonUtil.getObjectToString(list), UserListVO.class);
        PageResult<?> pageResult = new PageResult<>(result);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }


    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询用户信息")
    @GetMapping("/info/{id}")
    public UserListVO info(@PathVariable String id) {
        UserEntity entity = userService.getInfoByUserId(id);
        return JsonUtil.getJsonToBean(JsonUtil.getObjectToString(entity), UserListVO.class);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存用户")
    @PostMapping("/save")
    public Boolean save(@RequestBody UserEntity entity) throws CustomException {
        return userService.saveUpdate(entity);
    }

    /**
     * 重置密码
     *
     * @param changePwdInput 重置信息
     * @return 是否成功
     */
    @Operation(summary = "重置密码")
    @PostMapping("/changePwd")
    public Boolean changedPwd(@RequestBody ChangePwdInput changePwdInput) {
        return userService.changedPwd(changePwdInput);
    }

    /**
     * 解除锁定
     *
     * @param idsModel 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "解除锁定")
    @PostMapping("/unLock")
    public Boolean unLock(@RequestBody IdsModel idsModel) {
        return userService.unLock(idsModel);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除用户")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return userService.delete(ids);
    }
}

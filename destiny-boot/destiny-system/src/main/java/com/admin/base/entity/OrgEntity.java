package com.admin.base.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统组织表
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Data
@Table("base_basics_org")
@Accessors(chain = true)
@Schema(name = "系统组织表")
@EqualsAndHashCode(callSuper = false)
public class OrgEntity extends BaseEntity {

    /**
     * 父节点id
     */
    @Schema(description = "父节点id")
    @Column("parent_id")
    private String parentId;

    /**
     * 父节点名称
     */
    @Schema(description = "父节点名称")
    @Column("parent_name")
    private String parentName;

    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    @Column("name")
    private String name;

    /**
     * 简称
     */
    @Schema(description = "简称")
    @Column("short_name")
    private String shortName;

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column("code")
    private String code;

    /**
     * 类型
     */
    @Schema(description = "类型")
    @Column("type")
    private String type;

    /**
     * 状态
     */
    @Schema(description = "状态")
    @Column("state")
    private String state;

}

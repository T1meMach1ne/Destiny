package com.admin.base.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 角色信息表;
 *
 * @author hw_ren
 * @version 1.0
 * @since 2022/7/11
 */
@Data
@Table("base_basics_role")
@Accessors(chain = true)
@Schema(name = "角色信息表")
@EqualsAndHashCode(callSuper = true)
public class RoleEntity extends BaseEntity {

    /**
     * 角色编号
     */
    @Schema(description = "角色编号")
    @Column(value = "code")
    private String code;

    /**
     * 角色名称
     */
    @Schema(description = "角色名称")
    @Column(value = "name")
    private String name;

    /**
     * 角色类型
     */
    @Schema(description = "角色类型")
    @Column(value = "role_type")
    private String roleType;
}
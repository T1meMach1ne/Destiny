package com.admin.base.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.crypto.digest.DigestUtil;
import com.admin.base.entity.UserEntity;
import com.admin.base.model.auth.LoginInput;
import com.admin.base.service.AuthService;
import com.admin.base.service.UserService;
import com.admin.constant.DbNameConst;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.exception.CustomException;
import com.admin.model.UserInfo;
import com.admin.util.IpUtil;
import com.admin.util.ServletUtil;
import com.mybatisflex.annotation.UseDataSource;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserService userService;

    /**
     * 登录验证
     *
     * @param loginInput 登录参数
     * @return 当前用户
     */
    @Override
    public UserEntity checkLogin(LoginInput loginInput) {
        UserEntity entity = userService.getInfoByUserName(loginInput.getAccount());
        if (entity == null) {
            throw new CustomException(ErrorCodesEnum.A0201);
        }
        String md5Pwd = DigestUtil.md5Hex(loginInput.getAccount().toLowerCase() + loginInput.getPassword());
        if (!entity.getPassword().equals(md5Pwd)) {
            entity.setErrorCount(1);
            entity.setLockTime(LocalDateTime.now());
            userService.updateById(entity);
            throw new CustomException(ErrorCodesEnum.A401);
        }
        return entity;
    }

    /**
     * 根据userId获取用户信息
     *
     * @param userId 用户id
     * @return 用户信息
     */
    @Override
    public UserEntity getUserInfo(String userId) {
        UserEntity userEntityInfo = userService.getInfoByUserId(userId);
        if (userEntityInfo == null) {
            throw new CustomException(ErrorCodesEnum.A0201);
        }
        return userEntityInfo;
    }

    /**
     * 转化用户信息
     *
     * @param userInfo   需要转化的对象
     * @param userEntity 数据库源数据
     */
    @Override
    public void userInfo(UserInfo userInfo, UserEntity userEntity) {
        BeanUtil.copyProperties(userEntity, userInfo);
        userInfo.setId(userEntity.getId());
        userInfo.setLastLoginIp(IpUtil.getIpAddress());
        userInfo.setLastLoginPlatForm(ServletUtil.getUserAgent());
        userInfo.setLastLoginTime(LocalDateTime.now());
        //todo 设置菜单、权限、角色等缓存
    }
}

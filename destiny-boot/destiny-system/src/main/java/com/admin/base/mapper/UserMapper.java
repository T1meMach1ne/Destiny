package com.admin.base.mapper;

import com.admin.base.entity.UserEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Mapper
public interface UserMapper extends BaseMapper<UserEntity> {
}

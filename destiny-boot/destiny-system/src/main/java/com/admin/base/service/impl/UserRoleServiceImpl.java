package com.admin.base.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.admin.base.entity.UserRoleEntity;
import com.admin.base.mapper.UserRoleMapper;
import com.admin.base.service.UserRoleService;
import com.admin.constant.DbNameConst;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.update.UpdateWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户角色关系表;(base_user_role)表服务实现类
 *
 * @author hw_ren
 * @version 1.0
 * @since : 2022-7-11
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
public class UserRoleServiceImpl extends ServiceImpl<UserRoleMapper, UserRoleEntity> implements UserRoleService {

    /**
     * 给用户绑定角色
     *
     * @param userId     用户id
     * @param roleIdList 角色id集合
     * @return 是否成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean bindRole(String userId, List<String> roleIdList) {
        if (CollectionUtil.isEmpty(roleIdList)) {
            return false;
        }
        List<UserRoleEntity> list = new ArrayList<>();
        roleIdList.forEach(v -> list.add(new UserRoleEntity().setUserId(userId).setRoleId(v)));
        var entity = new UserRoleEntity();
        UpdateWrapper<UserRoleEntity> wrapper = UpdateWrapper.of(entity);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(UserRoleEntity::getUserId, userId);
        this.update(wrapper.toEntity(), queryWrapper);
        return this.saveBatch(list);
    }
}
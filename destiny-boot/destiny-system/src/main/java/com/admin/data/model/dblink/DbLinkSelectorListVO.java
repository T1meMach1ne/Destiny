package com.admin.data.model.dblink;

import com.admin.model.TreeNode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据连接表单对象
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DbLinkSelectorListVO extends TreeNode<DbLinkSelectorListVO> {

    /**
     * 连接名称
     */
    @Schema(description = "连接名称")
    private String name;

    /**
     * 连接驱动
     */
    @Schema(description = "连接驱动")
    private String dbType;

    /**
     * 连接驱动
     */
    @Schema(description = "主机名称")
    private String host;

    /**
     * 端口
     */
    @Schema(description = "端口")
    private String port;
}

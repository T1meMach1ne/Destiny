package com.admin.data.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.data.entity.FieldAreaDetailEntity;
import com.admin.data.entity.FieldAreaEntity;
import com.admin.data.mapper.FieldAreaMapper;
import com.admin.data.model.fieldarea.FieldAreaDetailFormVO;
import com.admin.data.model.fieldarea.FieldAreaFormVO;
import com.admin.data.service.FieldAreaDetailService;
import com.admin.data.service.FieldAreaService;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 数据域主表;(s_d_field_area)表服务实现类
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class FieldAreaServiceImpl extends ServiceImpl<FieldAreaMapper, FieldAreaEntity> implements FieldAreaService {
    private final FieldAreaDetailService fieldAreaDetailService;

    /**
     * 查询列表
     *
     * @return 列表数据
     */
    @Override
    public List<FieldAreaEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(FieldAreaEntity::getName, pageModel.getSearchValue()).or(
                    x -> {
                        x.like(FieldAreaEntity::getCode, pageModel.getSearchValue());
                    }
            );
        }
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(FieldAreaEntity::getCreateTime).desc();
        }
        Page<FieldAreaEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<FieldAreaEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    @Override
    public FieldAreaFormVO info(String id) {
        FieldAreaEntity entity = this.getById(id);
        FieldAreaFormVO vo = BeanUtil.toBean(entity, FieldAreaFormVO.class);
        List<FieldAreaDetailEntity> detailList = fieldAreaDetailService.getDetailList(List.of(id));
        vo.setDetail(BeanUtil.copyToList(detailList, FieldAreaDetailFormVO.class));
        return vo;
    }

    /**
     * 保存
     *
     * @param form 信息
     */
    @Override
    public void saveUpdate(FieldAreaFormVO form) {
        FieldAreaEntity entity = BeanUtil.toBean(form, FieldAreaEntity.class);
        if (ObjectUtil.isEmpty(form.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        this.saveOrUpdate(entity);
    }

    /**
     * 删除
     *
     * @param ids id数组
     */
    @Override
    public void delete(IdsModel ids) {
        List<FieldAreaEntity> list = this.listByIds(ids.getIds());
        List<FieldAreaDetailEntity> detailEntityList = fieldAreaDetailService.getDetailList(ids.getIds());
        this.removeByIds(list.stream().map(FieldAreaEntity::getId).toList());
        fieldAreaDetailService.removeByIds(detailEntityList.stream().map(FieldAreaDetailEntity::getId).toList());
    }
}

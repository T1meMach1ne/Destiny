package com.admin.data.service;

import com.admin.data.entity.LogEntity;
import com.admin.data.model.log.LogListPage;
import com.admin.model.IdsModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * 日志表;(s_log)表服务接口
 *
 * @author yh_liu
 * @version 1.0
 * @since : 2022-7-17
 */
public interface LogService extends IService<LogEntity> {

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<LogEntity> list(LogListPage pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    LogEntity info(String id);

    /**
     * 保存
     *
     * @param entity 当前信息
     * @return 是否成功
     */
    Boolean saveUpdate(LogEntity entity);

    /**
     * 删除
     *
     * @param ids 角色id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);

    /**
     * 转化日志信息
     *
     * @param userId   用户id
     * @param logType  日志类型
     * @param logGrade 日志等级
     * @param data     日志数据
     * @return 日志信息
     */
    LogEntity getLogData(Long userId, String logType, String logGrade, String data);


    /**
     * 转化日志信息
     *
     * @param logType  日志类型
     * @param logGrade 日志等级
     * @param data     日志数据
     * @return 日志信息
     */
    LogEntity getLogData(String logType, String logGrade, String data);
}
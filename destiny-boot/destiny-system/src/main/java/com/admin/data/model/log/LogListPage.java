package com.admin.data.model.log;

import com.admin.model.PageModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDateTime;

/**
 * @author yh_liu
 * @since 2022-7-31
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class LogListPage extends PageModel {

    /**
     * 日志类型
     */
    private String logType;

    /**
     * 开始事件
     */
    private LocalDateTime startTime;

    /**
     * 结束事件
     */
    private LocalDateTime endTime;
}

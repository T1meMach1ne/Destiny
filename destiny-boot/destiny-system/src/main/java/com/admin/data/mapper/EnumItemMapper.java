package com.admin.data.mapper;

import com.admin.data.entity.EnumItemEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/7/25
 */
@Mapper
public interface EnumItemMapper extends BaseMapper<EnumItemEntity> {
}
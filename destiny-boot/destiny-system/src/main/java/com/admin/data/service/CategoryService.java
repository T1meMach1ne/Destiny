package com.admin.data.service;

import com.admin.data.entity.CategoryEntity;
import com.admin.data.model.category.CategoryTreeVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/30
 */
public interface CategoryService extends IService<CategoryEntity> {

    /**
     * 查询所有列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<CategoryTreeVO> tree(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    CategoryEntity info(String id);

    /**
     * 保存
     *
     * @param entity 组织信息
     * @return 是否成功
     */
    boolean saveUpdate(CategoryEntity entity);

    /**
     * 删除
     *
     * @param ids 组织id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);
}

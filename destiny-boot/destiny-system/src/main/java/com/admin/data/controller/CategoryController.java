package com.admin.data.controller;


import com.admin.data.entity.CategoryEntity;
import com.admin.data.model.category.CategoryTreeVO;
import com.admin.data.service.CategoryService;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 系统类别 前端控制器
 *
 * @author yh_liu
 * @since 2022-07-30
 */
@Tag(name = "category", description = "系统类别")
@RestController
@RequestMapping("/api/base/data/category")
@RequiredArgsConstructor
public class CategoryController {
    private final CategoryService categoryService;

    /**
     * 查询列表
     *
     * @return 列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public List<CategoryEntity> list() {
        return categoryService.list();
    }

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/tree")
    public List<CategoryTreeVO> tree(PageModel pageModel) {
        return categoryService.tree(pageModel);
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询类别信息")
    @GetMapping("/info/{id}")
    public CategoryEntity info(@PathVariable String id) {
        return categoryService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     */
    @Operation(summary = "保存类别")
    @PostMapping("/save")
    public Boolean save(@RequestBody CategoryEntity entity) {
        return categoryService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除类别")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return categoryService.delete(ids);
    }
}

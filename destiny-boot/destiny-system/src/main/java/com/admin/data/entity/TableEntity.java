package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 数据域主表
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/8/16
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table("base_data_table")
@Schema(name = "表设计对象", description = "表设计表")
public class TableEntity extends BaseEntity {

    /**
     * 库名
     */
    @Schema(description = "库名")
    @Column(value = "db_name")
    private String dbName;

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column(value = "code")
    private String code;

    /**
     * 表名
     */
    @Schema(description = "表名")
    @Column(value = "table_Code")
    private String tableCode;

    /**
     * 显示名称
     */
    @Schema(description = "显示名称")
    @Column(value = "table_name")
    private String tableName;
}

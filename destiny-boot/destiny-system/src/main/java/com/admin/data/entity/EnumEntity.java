package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统枚举主表
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/7/24
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table("base_data_enum")
@Schema(name = "枚举对象", description = "系统枚举表")
public class EnumEntity extends BaseEntity {

    /**
     * 枚举编号
     */
    @Schema(description = "枚举编号")
    @Column(value = "code")
    private String code;

    /**
     * 枚举名称
     */
    @Schema(description = "枚举名称")
    @Column(value = "name")
    private String name;

    /**
     * 枚举类型
     */
    @Schema(description = "枚举类型")
    @Column(value = "type")
    private String type;

    /**
     * 枚举库
     */
    @Schema(description = "枚举库")
    @Column(value = "db_name")
    private String dbName;

    /**
     * 枚举SQL
     */
    @Schema(description = "枚举SQL")
    @Column(value = "db_sql")
    private String dbSql;
}

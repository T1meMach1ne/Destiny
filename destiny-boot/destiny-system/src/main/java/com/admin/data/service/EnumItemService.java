package com.admin.data.service;

import com.admin.data.entity.EnumItemEntity;
import com.admin.data.model.enums.EnumItemPage;
import com.admin.exception.CustomException;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/7/25
 */
public interface EnumItemService extends IService<EnumItemEntity> {

    /**
     * 根据查询参数查询枚举项明细
     *
     * @param pageModel 查询参数
     * @return 枚举列表
     * @throws Exception 异常
     */
    List<EnumItemEntity> list(EnumItemPage pageModel) throws Exception;

    /**
     * 根据枚举id查询枚举项明细
     *
     * @param enumId 枚举id
     * @return 枚举列表
     * @throws Exception 异常
     */
    List<EnumItemEntity> allList(String enumId) throws Exception;

    /**
     * 新增和修改
     *
     * @param entity 当前信息
     * @return 是否保存成功
     * @throws CustomException 异常
     */
    boolean saveUpdate(EnumItemEntity entity) throws CustomException;

    /**
     * 删除枚举项操作
     *
     * @param ids 枚举项的ID
     * @return 反馈是否成功
     */
    boolean delete(IdsModel ids);

    /**
     * 根据ID查询枚举项单条信息
     *
     * @param id 枚举的ID
     * @return 返回信息
     */
    EnumItemEntity info(String id);

    /**
     * 根据逗号分隔的ids查询枚举项多条信息
     *
     * @param codes 枚举编号集合
     * @return 返回信息
     * @throws Exception 异常
     */
    List<EnumVO> getEnumItemList(String codes) throws Exception;

}

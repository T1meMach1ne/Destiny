package com.admin.data.service.impl;

import com.admin.constant.DbNameConst;
import com.admin.constant.SymbolConstant;
import com.admin.data.entity.DataTypeDetailEntity;
import com.admin.data.mapper.DataTypeDetailMapper;
import com.admin.data.service.DataTypeDetailService;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class DataTypeDetailServiceImpl extends ServiceImpl<DataTypeDetailMapper, DataTypeDetailEntity> implements DataTypeDetailService {
    /**
     * 根据主表id获取明细
     *
     * @param ids 主表id
     * @return 子表信息
     */
    @Override
    public List<DataTypeDetailEntity> getDetailList(List<String> ids) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(DataTypeDetailEntity::getDetailId, ids);
        return this.list(queryWrapper);
    }

    /**
     * 根据id获取明细
     *
     * @param ids 明细表id
     * @return 子表信息
     */
    @Override
    public List<DataTypeDetailEntity> getList(String ids) {
        List<String> listIds = Arrays.asList(ids.split(SymbolConstant.STRING_COMMA));
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(DataTypeDetailEntity::getId, listIds);
        return this.list(queryWrapper);
    }

}

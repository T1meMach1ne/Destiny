package com.admin.data.mapper;

import com.admin.data.entity.TableEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/8/16
 */
@Mapper
public interface TableMapper extends BaseMapper<TableEntity> {
}

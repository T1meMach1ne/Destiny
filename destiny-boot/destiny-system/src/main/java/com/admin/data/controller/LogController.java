package com.admin.data.controller;


import com.admin.data.entity.LogEntity;
import com.admin.data.model.log.LogListPage;
import com.admin.data.service.LogService;
import com.admin.model.IdsModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 日志管理 前端控制器
 *
 * @author yh_liu
 * @since 2022-07-31
 */
@Tag(name = "log", description = "日志管理")
@RestController
@RequestMapping("/api/base/data/log")
@RequiredArgsConstructor
public class LogController {

    private final LogService logService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<?> list(LogListPage pageModel) {
        List<LogEntity> list = logService.list(pageModel);
        PageResult<?> pageResult = new PageResult<>(list);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询信息")
    @GetMapping("/info/{id}")
    public LogEntity info(@PathVariable String id) {
        return logService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     */
    @Operation(summary = "保存")
    @PostMapping("/save")
    public Boolean save(@RequestBody LogEntity entity) {
        return logService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return logService.delete(ids);
    }
}

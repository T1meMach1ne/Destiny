package com.admin.data.service;

import com.admin.data.entity.DataTypeEntity;
import com.admin.data.model.datatype.DataTypeVO;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
public interface DataTypeService extends IService<DataTypeEntity> {

    /**
     * 查询所有列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<DataTypeEntity> list(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    DataTypeVO info(String id);

    /**
     * 保存
     *
     * @param entity 信息
     */
    void saveUpdate(DataTypeVO entity);

    /**
     * 删除
     *
     * @param ids id数组
     */
    void delete(IdsModel ids);

    /**
     * 获取数据类型枚举
     *
     * @return 枚举
     */
    List<EnumVO> getDataTypeEnum();
}

package com.admin.data.controller;

import com.admin.data.entity.EnumItemEntity;
import com.admin.data.model.enums.EnumItemPage;
import com.admin.data.service.EnumItemService;
import com.admin.data.service.EnumService;
import com.admin.exception.CustomException;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 枚举管理
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/7/25
 */
@Tag(name = "enumItem", description = "枚举项管理")
@RestController
@RequestMapping("/api/base/data/enumItem")
@RequiredArgsConstructor
public class EnumItemController {
    /**
     * 枚举子项接口
     */
    public final EnumItemService enumListService;
    /**
     * 枚举接口
     */
    public final EnumService enumService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     * @throws Exception 异常信息
     */
    @Operation(summary = "查询枚举列表")
    @GetMapping("/list")
    public PageResult<?> list(EnumItemPage pageModel) throws Exception {
        List<EnumItemEntity> enumList = enumListService.list(pageModel);
        PageResult<?> pageResult = new PageResult<>(enumList);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存枚举")
    @PostMapping("/save")
    public Boolean save(@RequestBody EnumItemEntity entity) throws CustomException {
        return enumListService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除枚举")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return enumListService.delete(ids);
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询单条枚举")
    @GetMapping("/info/{id}")
    public EnumItemEntity info(@PathVariable String id) {
        return enumListService.info(id);
    }

    /**
     * 多个code查询子项
     *
     * @param codes 逗号分隔枚举编号
     * @return 枚举子项列表
     * @throws Exception 异常信息
     */
    @Operation(summary = "根据编号分隔查询多个批量的枚举")
    @PostMapping("/getEnumItemList")
    public List<EnumVO> getEnumItemList(@RequestBody String codes) throws Exception {
        return enumListService.getEnumItemList(codes);
    }
}

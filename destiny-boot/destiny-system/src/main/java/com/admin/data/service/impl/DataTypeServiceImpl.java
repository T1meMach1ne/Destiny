package com.admin.data.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.constant.SymbolConstant;
import com.admin.data.entity.DataTypeDetailEntity;
import com.admin.data.entity.DataTypeEntity;
import com.admin.data.mapper.DataTypeMapper;
import com.admin.data.model.datatype.DataTypeDetailVO;
import com.admin.data.model.datatype.DataTypeVO;
import com.admin.data.service.DataTypeDetailService;
import com.admin.data.service.DataTypeService;
import com.admin.model.BaseEntity;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class DataTypeServiceImpl extends ServiceImpl<DataTypeMapper, DataTypeEntity> implements DataTypeService {

    private final DataTypeDetailService dataTypeDetailService;

    /**
     * 查询列表
     *
     * @return 列表数据
     */
    @Override
    public List<DataTypeEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(DataTypeEntity::getName, pageModel.getSearchValue()).or(
                    x -> {
                        x.like(DataTypeEntity::getCode, pageModel.getSearchValue());
                    }
            );
        }
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(DataTypeEntity::getSortIndex).asc();
        }
        Page<DataTypeEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<DataTypeEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 获取信息
     *
     * @param id 查询参数
     * @return 当前数据
     */
    @Override
    public DataTypeVO info(String id) {
        DataTypeEntity entity = this.getById(id);
        DataTypeVO vo = BeanUtil.toBean(entity, DataTypeVO.class);
        List<DataTypeDetailEntity> detailList = dataTypeDetailService.getDetailList(List.of(id));
        vo.setDetail(BeanUtil.copyToList(detailList, DataTypeDetailVO.class));
        return vo;
    }

    /**
     * 保存
     *
     * @param form 当前信息
     */
    @Override
    public void saveUpdate(DataTypeVO form) {
        DataTypeEntity entity = BeanUtil.toBean(form, DataTypeEntity.class);
        if (ObjectUtil.isEmpty(form.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        this.saveOrUpdate(entity);

        List<DataTypeDetailEntity> removeList = dataTypeDetailService.getList(entity.getId());
        if (!form.getDetail().isEmpty()) {
            List<DataTypeDetailEntity> detailList = BeanUtil.copyToList(form.getDetail(), DataTypeDetailEntity.class);
            List<String> inputIds = detailList.stream().map(BaseEntity::getId).filter(StringUtils::isNotEmpty).toList();
            List<DataTypeDetailEntity> existList = dataTypeDetailService.getDetailList(List.of(entity.getId()));
            List<String> existIds = existList.stream().map(BaseEntity::getId).toList();
            String notExistIds = existIds.stream().filter(x -> !inputIds.contains(x)).collect(Collectors.joining(SymbolConstant.STRING_COMMA));
            removeList = dataTypeDetailService.getList(notExistIds);
            if (!form.getDetail().isEmpty()) {
                for (DataTypeDetailEntity item : detailList) {
                    if (ObjectUtil.isEmpty(item.getId())) {
                        item.setId(GuIdUtil.nextId());
                        item.setDetailId(entity.getId());
                    }
                }
                if (!detailList.isEmpty()) {
                    dataTypeDetailService.saveOrUpdateBatch(detailList);
                }
            }
            dataTypeDetailService.removeByIds(removeList.stream().map(BaseEntity::getId).toList());
        }
        dataTypeDetailService.removeByIds(removeList.stream().map(BaseEntity::getId).toList());
    }

    /**
     * 删除
     *
     * @param idsModel id数组
     */
    @Override
    public void delete(IdsModel idsModel) {
        List<DataTypeEntity> list = this.listByIds(idsModel.getIds());
        List<DataTypeDetailEntity> detailList = dataTypeDetailService.getDetailList(idsModel.getIds());
        this.removeByIds(list.stream().map(BaseEntity::getId).toList());
        dataTypeDetailService.removeByIds(detailList.stream().map(BaseEntity::getId).toList());
    }

    /**
     * 获取数据类型枚举
     *
     * @return 枚举
     */
    @Override
    public List<EnumVO> getDataTypeEnum() {
        List<EnumVO> enumVOList = new ArrayList<>();
        QueryWrapper queryWrapper = new QueryWrapper();
        List<DataTypeEntity> dataTypeEntityList = this.list(queryWrapper);
        for (DataTypeEntity item : dataTypeEntityList) {
            List<DataTypeDetailEntity> detailList = dataTypeDetailService.getDetailList(List.of(item.getId()));
            //此处暂时只支持“MySql”，我认为使用什么数据库应该写在全局变量之中
            DataTypeDetailEntity dataTypeDetail = detailList.stream().filter(x -> Objects.equals(x.getName(), "MySql")).findFirst().orElse(null);
            if (dataTypeDetail != null) {
                EnumVO enumVO = new EnumVO();
                enumVO.setKey(dataTypeDetail.getCode());
                enumVO.setValue(item.getName());
                enumVOList.add(enumVO);
            }
        }
        return enumVOList;
    }
}

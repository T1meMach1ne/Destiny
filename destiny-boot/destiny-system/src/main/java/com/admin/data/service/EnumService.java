package com.admin.data.service;

import com.admin.data.entity.EnumEntity;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/7/24
 */
public interface EnumService extends IService<EnumEntity> {

    /**
     * 根据查询参数查询枚举列表
     *
     * @param pageModel 查询参数
     * @return 枚举列表
     */
    List<EnumEntity> list(PageModel pageModel);

    /**
     * 新增和修改
     *
     * @param entity 当前信息
     * @return 是否保存成功
     * @throws CustomException 错误信息
     */
    boolean saveUpdate(EnumEntity entity) throws CustomException;

    /**
     * 删除枚举信息操作
     *
     * @param ids 枚举的ID
     * @return 反馈是否成功
     */
    boolean delete(IdsModel ids);

    /**
     * 根据ID查询单条信息
     *
     * @param id 枚举的ID
     * @return 返回信息
     */
    EnumEntity info(String id);

    /**
     * 根据code取到枚举信息集合
     *
     * @param codes 枚举code逗号分隔
     * @return 枚举信息集合
     */
    List<EnumEntity> getEnumByCode(String codes);

}

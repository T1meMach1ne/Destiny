package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统枚举明细表
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/7/25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table("base_data_enum_item")
@Schema(name = "枚举对象", description = "系统枚举表")
public class EnumItemEntity extends BaseEntity {

    /**
     * 枚举子项编号
     */
    @Schema(description = "枚举子项编号")
    @Column(value = "code")
    private String code;

    /**
     * 枚举子项值
     */
    @Schema(description = "枚举子项值")
    @Column(value = "name")
    private String name;

    /**
     * 枚举从属ID
     */
    @Schema(description = "枚举从属ID")
    @Column(value = "source_id")
    private String sourceId;

    /**
     * 枚举联动分类
     */
    @Schema(description = "枚举联动分类")
    @Column(value = "category")
    private String category;
}

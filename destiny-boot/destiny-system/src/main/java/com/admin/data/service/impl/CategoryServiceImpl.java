package com.admin.data.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.data.entity.CategoryEntity;
import com.admin.data.mapper.CategoryMapper;
import com.admin.data.model.category.CategoryTreeVO;
import com.admin.data.service.CategoryService;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.admin.util.TreeUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/30
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, CategoryEntity> implements CategoryService {

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<CategoryTreeVO> tree(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        List<CategoryEntity> treeData = this.list(queryWrapper);
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            treeData = treeData.stream().filter(t -> StrUtil.nullToEmpty(t.getName()).contains(pageModel.getSearchValue())
                    || StrUtil.nullToEmpty(t.getCode()).contains(pageModel.getSearchValue())).collect(Collectors.toList());
        }
        List<CategoryTreeVO> modeList = BeanUtil.copyToList(treeData, CategoryTreeVO.class);
        return TreeUtil.build(modeList);
    }

    /**
     * 获取信息
     *
     * @param id 查询参数
     * @return 当前数据
     */
    @Override
    public CategoryEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 保存
     *
     * @param entity 组织信息
     * @return 是否成功
     */
    @Override
    public boolean saveUpdate(CategoryEntity entity) {
        if (ObjectUtil.isEmpty(entity.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        return this.saveOrUpdate(entity);
    }

    /**
     * 删除
     *
     * @param idsModel 组织id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel idsModel) {
        List<CategoryEntity> list = this.listByIds(idsModel.getIds());
        return this.removeByIds(list.stream().map(CategoryEntity::getId).toList());
    }
}

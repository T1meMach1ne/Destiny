package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 数据域子表
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table("base_data_field_area_detail")
@Schema(name = "数据域对象", description = "数据域子表")
public class FieldAreaDetailEntity extends BaseEntity {

    /**
     * 所属ID
     */
    @Schema(description = "所属ID")
    @Column(value = "field_id")
    private String fieldId;

    /**
     * 数据库类型名称
     */
    @Schema(description = "数据库类型名称")
    @Column(value = "db_name")
    private String dbName;

    /**
     * 数据域代码
     */
    @Schema(description = "数据域代码")
    @Column(value = "field_code")
    private String fieldCode;

}

package com.admin.data.controller;


import com.admin.data.entity.DataTypeEntity;
import com.admin.data.model.datatype.DataTypeVO;
import com.admin.data.service.DataTypeService;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 数据类型定义 前端控制器
 *
 * @author yh_liu
 * @since 2022-07-21
 */
@Tag(name = "dataType", description = "数据类型定义")
@RestController
@RequestMapping("/api/base/data/dataType")
@RequiredArgsConstructor
public class DataTypeController {

    private final DataTypeService dataTypeService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<DataTypeEntity> list(PageModel pageModel) {
        List<DataTypeEntity> list = dataTypeService.list(pageModel);
        PageResult<DataTypeEntity> pageResult = new PageResult<>(list);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询信息")
    @GetMapping("/info/{id}")
    public DataTypeVO info(@PathVariable String id) {
        return dataTypeService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     */
    @Operation(summary = "保存")
    @PostMapping("/save")
    public void save(@RequestBody DataTypeVO entity) {
        dataTypeService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     */
    @Operation(summary = "删除")
    @PostMapping("/delete")
    public void delete(@RequestBody IdsModel ids) {
        dataTypeService.delete(ids);
    }

    /**
     * 获取数据类型枚举
     *
     * @return 列表
     */
    @Operation(summary = "获取数据类型枚举")
    @GetMapping("/getDataType")
    public List<EnumVO> getDataType() {
        return dataTypeService.getDataTypeEnum();
    }
}

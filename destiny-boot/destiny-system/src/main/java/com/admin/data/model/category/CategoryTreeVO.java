package com.admin.data.model.category;

import com.admin.model.TreeNode;
import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/30
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class CategoryTreeVO extends TreeNode<CategoryTreeVO> {

    /**
     * 名称
     */
    @Schema(description = "名称")
    @Column("name")
    private String name;

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column("code")
    private String code;

    /**
     * 类型
     */
    @Schema(description = "类型")
    @Column("type")
    private String type;

    /**
     * 状态
     */
    @Schema(description = "状态")
    @Column("state")
    private String state;
}

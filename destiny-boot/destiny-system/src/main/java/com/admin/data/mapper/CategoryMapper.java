package com.admin.data.mapper;

import com.admin.data.entity.CategoryEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/30
 */
@Mapper
public interface CategoryMapper extends BaseMapper<CategoryEntity> {
}
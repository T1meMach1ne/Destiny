package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 数据域主表
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Table("base_data_field_area")
@Schema(name = "数据域对象", description = "数据域表")
public class FieldAreaEntity extends BaseEntity {

    /**
     * 数据域代码
     */
    @Schema(description = "数据域代码")
    @Column(value = "code")
    private String code;

    /**
     * 显示名称
     */
    @Schema(description = "显示名称")
    @Column(value = "name")
    private String name;

    /**
     * 数据类型
     */
    @Schema(description = "数据类型")
    @Column(value = "field_type")
    private String fieldType;

    /**
     * 数据类型编号
     */
    @Schema(description = "数据类型编号")
    @Column(value = "field_code")
    private String fieldCode;

    /**
     * 长度
     */
    @Schema(description = "长度")
    @Column(value = "field_length")
    private Integer fieldLength;

    /**
     * 小数位数
     */
    @Schema(description = "小数位数")
    @Column(value = "field_decimal_length")
    private Integer fieldDecimalLength;

}

package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 基础日志表;
 *
 * @author yh_Liu
 * @version 1.0
 * @since : 2022-7-17
 */
@Data
@Table("base_data_log")
@Accessors(chain = true)
@Schema(name = "登录日志表")
@EqualsAndHashCode(callSuper = true)
public class LogEntity extends BaseEntity {

    /**
     * 登录人id
     */
    @Schema(description = "登录人id")
    @Column(value = "login_user_id")
    private String loginUserId;

    /**
     * 登录人
     */
    @Schema(description = "登录人")
    @Column(value = "login_user_name")
    private String loginUserName;

    /**
     * 登录ip
     */
    @Schema(description = "登录ip")
    @Column(value = "login_ip")
    private String loginIp;

    /**
     * 浏览器
     */
    @Schema(description = "浏览器")
    @Column(value = "user_agent")
    private String userAgent;

    /**
     * 日志类型
     */
    @Schema(description = "日志类型")
    @Column(value = "log_type")
    private String logType;

    /**
     * 日志等级
     */
    @Schema(description = "日志等级")
    @Column(value = "log_grade")
    private String logGrade;

    /**
     * 请求地址
     */
    @Schema(description = "请求地址")
    @Column(value = "log_url")
    private String logUrl;

    /**
     * 日志数据
     */
    @Schema(description = "日志数据")
    @Column(value = "data")
    private String data;

    /**
     * 附加信息
     */
    @Schema(description = "附加信息")
    @Column(value = "log_content")
    private String logContent;
}

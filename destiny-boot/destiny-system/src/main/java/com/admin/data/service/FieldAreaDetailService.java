package com.admin.data.service;

import com.admin.data.entity.FieldAreaDetailEntity;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
public interface FieldAreaDetailService extends IService<FieldAreaDetailEntity> {
    /**
     * 根据主表id获取明细
     *
     * @param id 主表id
     * @return 子表信息
     */
    List<FieldAreaDetailEntity> getDetailList(List<String> id);

    /**
     * 根据id获取明细
     *
     * @param id 明细表id
     * @return 子表信息
     */
    List<FieldAreaDetailEntity> getList(String id);
}

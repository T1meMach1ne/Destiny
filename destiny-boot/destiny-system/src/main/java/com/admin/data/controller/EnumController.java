package com.admin.data.controller;

import com.admin.data.entity.EnumEntity;
import com.admin.data.service.EnumService;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 枚举管理
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/7/24
 */
@Tag(name = "enum", description = "枚举管理")
@RestController
@RequestMapping("/api/base/data/enum")
@RequiredArgsConstructor
public class EnumController {
    private final EnumService enumService;

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存枚举")
    @PostMapping("/save")
    public Boolean save(@RequestBody EnumEntity entity) throws CustomException {
        return enumService.saveUpdate(entity);
    }

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询枚举列表")
    @GetMapping("/list")
    public PageResult<?> list(PageModel pageModel) {
        List<EnumEntity> enumList = enumService.list(pageModel);
        PageResult<?> pageResult = new PageResult<>(enumList);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除枚举")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return enumService.delete(ids);
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询单条枚举")
    @GetMapping("/info/{id}")
    public EnumEntity info(@PathVariable String id) {
        return enumService.info(id);
    }


    /**
     * 多个code查询枚举
     *
     * @param codes 逗号分隔枚举编号
     * @return 枚举列表
     */
    @Operation(summary = "多个code查询")
    @PostMapping("/getEnumByCode")
    public List<EnumEntity> getEnumByCode(@PathVariable String codes) {
        return enumService.getEnumByCode(codes);
    }
}

package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统数据类型明细表
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Data
@Table("base_data_type_detail")
@Accessors(chain = true)
@Schema(name = "系统数据类型明细表")
@EqualsAndHashCode(callSuper = false)
public class DataTypeDetailEntity extends BaseEntity {

    /**
     * 主表id
     */
    @Schema(description = "主表id")
    @Column("detail_id")
    private String detailId;

    /**
     * 数据库名称
     */
    @Schema(description = "数据库名称")
    @Column("name")
    private String name;

    /**
     * 字段类型名称
     */
    @Schema(description = "字段类型名称")
    @Column("code")
    private String code;

}

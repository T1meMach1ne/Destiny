package com.admin.data.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.data.entity.TableColumnEntity;
import com.admin.data.entity.TableEntity;
import com.admin.data.mapper.TableMapper;
import com.admin.data.model.table.TableColumnVO;
import com.admin.data.model.table.TableVO;
import com.admin.data.service.TableColumnService;
import com.admin.data.service.TableService;
import com.admin.model.BaseEntity;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class TableServiceImpl extends ServiceImpl<TableMapper, TableEntity> implements TableService {

    private final TableColumnService tableColumnService;

    /**
     * 查询列表
     *
     * @return 列表数据
     */
    @Override
    public List<TableEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(TableEntity::getTableName, pageModel.getSearchValue()).or(
                    x -> {
                        x.like(TableEntity::getCode, pageModel.getSearchValue());
                    }
            );
        }
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(TableEntity::getCreateTime, Boolean.FALSE);
        }
        Page<TableEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<TableEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 获取信息
     *
     * @param id 查询参数
     * @return 当前数据
     */
    @Override
    public TableVO info(String id) {
        TableEntity entity = this.getById(id);
        TableVO vo = BeanUtil.toBean(entity, TableVO.class);
        List<TableColumnEntity> detailList = tableColumnService.getDetailList(List.of(id));
        vo.setColumn(BeanUtil.copyToList(detailList, TableColumnVO.class));
        return vo;
    }

    /**
     * 保存
     *
     * @param form 当前信息
     */
    @Override
    public void saveUpdate(TableVO form) {
        TableEntity entity = BeanUtil.toBean(form, TableEntity.class);
        if (ObjectUtil.isEmpty(form.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        this.saveOrUpdate(entity);

        List<TableColumnEntity> removeList = tableColumnService.getList(List.of(entity.getId()));
        if (!form.getColumn().isEmpty()) {
            List<TableColumnEntity> detailList = BeanUtil.copyToList(form.getColumn(), TableColumnEntity.class);
            List<String> inputIds = detailList.stream().map(BaseEntity::getId).filter(StringUtils::isNotEmpty).toList();
            List<TableColumnEntity> existList = tableColumnService.getDetailList(List.of(entity.getId()));
            List<String> existIds = existList.stream().map(BaseEntity::getId).toList();
            List<String> notExistIds = existIds.stream().filter(x -> !inputIds.contains(x)).toList();
            removeList = tableColumnService.getList(notExistIds);
            if (!form.getColumn().isEmpty()) {
                for (TableColumnEntity item : detailList) {
                    if (ObjectUtil.isEmpty(item.getId())) {
                        item.setId(GuIdUtil.nextId());
                        item.setTableId(entity.getId());
                    }
                }
                if (!detailList.isEmpty()) {
                    tableColumnService.saveOrUpdateBatch(detailList);
                }
            }
            tableColumnService.removeByIds(removeList.stream().map(BaseEntity::getId).toList());
        }
        tableColumnService.removeByIds(removeList.stream().map(BaseEntity::getId).toList());
    }

    /**
     * 删除
     *
     * @param ids id数组
     */
    @Override
    public void delete(IdsModel ids) {
        List<TableEntity> list = this.listByIds(ids.getIds());
        List<TableColumnEntity> detailList = tableColumnService.getDetailList(ids.getIds());
        this.removeByIds(list.stream().map(BaseEntity::getId).toList());
        tableColumnService.removeByIds(detailList.stream().map(BaseEntity::getId).toList());
    }

}

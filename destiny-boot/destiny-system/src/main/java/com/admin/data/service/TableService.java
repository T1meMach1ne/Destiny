package com.admin.data.service;

import com.admin.data.entity.TableEntity;
import com.admin.data.model.table.TableVO;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/8/16
 */
public interface TableService extends IService<TableEntity> {
    /**
     * 查询所有列表
     *
     * @param pageModel 分页对象
     * @return 列表数据
     */
    List<TableEntity> list(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    TableVO info(String id);

    /**
     * 保存
     *
     * @param entity 信息
     */
    void saveUpdate(TableVO entity);

    /**
     * 删除
     *
     * @param ids id数组
     */
    void delete(IdsModel ids);
}

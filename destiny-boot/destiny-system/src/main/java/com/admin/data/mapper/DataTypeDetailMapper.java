package com.admin.data.mapper;

import com.admin.data.entity.DataTypeDetailEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/8/6
 */
@Mapper
public interface DataTypeDetailMapper extends BaseMapper<DataTypeDetailEntity> {
}
package com.admin.data.service;

import com.admin.data.entity.TableColumnEntity;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022-08-16
 */
public interface TableColumnService extends IService<TableColumnEntity> {
    /**
     * 根据主表id获取明细
     *
     * @param id 主表id
     * @return 字段信息
     */
    List<TableColumnEntity> getDetailList(List<String> id);

    /**
     * 根据id获取明细
     *
     * @param id 明细表id
     * @return 字段信息
     */
    List<TableColumnEntity> getList(List<String> id);
}

package com.admin.data.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.data.entity.LogEntity;
import com.admin.data.mapper.LogMapper;
import com.admin.data.model.log.LogListPage;
import com.admin.data.service.LogService;
import com.admin.enums.system.FieldOrderEnum;
import com.admin.model.BaseEntity;
import com.admin.model.IdsModel;
import com.admin.model.UserInfo;
import com.admin.util.CurrentUtil;
import com.admin.util.GuIdUtil;
import com.admin.util.IpUtil;
import com.admin.util.ServletUtil;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 日志表;(s_log)表服务实现类
 *
 * @author yh_liu
 * @version 1.0
 * @since : 2022-7-31
 */
@Service
@RequiredArgsConstructor
public class LogServiceImpl extends ServiceImpl<LogMapper, LogEntity> implements LogService {

    private final CurrentUtil currentUtil;

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<LogEntity> list(LogListPage pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(LogEntity::getLogType, pageModel.getLogType());
        if (ObjectUtil.isNotEmpty(pageModel.getStartTime()) && ObjectUtil.isNotEmpty(pageModel.getEndTime())) {
            queryWrapper.ge(LogEntity::getCreateTime, pageModel.getStartTime()).le(LogEntity::getCreateTime, pageModel.getEndTime());
        }
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            String searchValue = pageModel.getSearchValue();
            queryWrapper.and(t -> {
                t.like(BaseEntity::getCreateUserName, searchValue)
                        .or(x -> {
                            x.like(LogEntity::getLoginIp, searchValue);
                        }).or(x -> {
                            x.like(LogEntity::getLogContent, searchValue);
                        });
            });
        }
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(LogEntity::getCreateTime, Boolean.FALSE);
        } else {
            queryWrapper = FieldOrderEnum.ASC.getKey().equalsIgnoreCase(pageModel.getSortOrder()) ?
                    queryWrapper.orderBy(pageModel.getSortField(), Boolean.TRUE) : queryWrapper.orderBy(pageModel.getSortField(), Boolean.FALSE);
        }
        Page<LogEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<LogEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 获取信息
     *
     * @param id 主键id
     * @return 当前数据
     */
    @Override
    public LogEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 保存
     *
     * @param entity 当前信息
     * @return 是否成功
     */
    @Override
    public Boolean saveUpdate(LogEntity entity) {
        if (ObjectUtil.isEmpty(entity.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        return this.saveOrUpdate(entity);
    }

    /**
     * 删除
     *
     * @param ids id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel ids) {
        List<LogEntity> list = this.listByIds(ids.getIds());
        return this.removeByIds(list.stream().map(LogEntity::getId).toList());
    }

    /**
     * 转化日志信息
     *
     * @param userId   用户id
     * @param logType  日志类型
     * @param logGrade 日志等级
     * @param data     日志数据
     * @return 日志信息
     */
    @Override
    public LogEntity getLogData(Long userId, String logType, String logGrade, String data) {
        UserInfo userInfo = currentUtil.getUserInfo(userId);
        return getLogEntity(logType, logGrade, data, userInfo);
    }

    /**
     * 转化日志信息
     *
     * @param logType  日志类型
     * @param logGrade 日志等级
     * @param data     日志数据
     * @return 日志信息
     */
    @Override
    public LogEntity getLogData(String logType, String logGrade, String data) {
        UserInfo userInfo = currentUtil.getUserInfo();
        return getLogEntity(logType, logGrade, data, userInfo);
    }

    @NotNull
    private LogEntity getLogEntity(String logType, String logGrade, String data, UserInfo userInfo) {
        LogEntity logEntity = new LogEntity();
        logEntity.setId(GuIdUtil.nextId());
        logEntity.setCreateTime(LocalDateTime.now());
        logEntity.setCreateUser(userInfo.getId());
        logEntity.setCreateUserName(userInfo.getUserName());
        logEntity.setCreateOrg(userInfo.getOrgId());
        logEntity.setLoginIp(IpUtil.getIpAddress());
        logEntity.setLogUrl(ServletUtil.getServletPath());
        logEntity.setLoginUserId(userInfo.getId());
        logEntity.setLoginUserName(userInfo.getUserName());
        logEntity.setUserAgent(ServletUtil.getUserAgent());
        logEntity.setLogType(logType);
        logEntity.setLogGrade(logGrade);
        logEntity.setData(data);
        return logEntity;
    }
}
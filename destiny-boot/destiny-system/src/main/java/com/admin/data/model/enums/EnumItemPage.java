package com.admin.data.model.enums;

import com.admin.model.PageModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hw_ren
 * @since 2022-7-26
 */

@EqualsAndHashCode(callSuper = true)
@Data
public class EnumItemPage extends PageModel {

    /**
     * 枚举id
     */
    @Schema(description = "枚举id")
    private String enumId;

}

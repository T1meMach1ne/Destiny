package com.admin.data.model.dblink;

import com.admin.model.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DbLinkListVO extends BaseEntity {

    /**
     * 连接名称
     */
    @Schema(description = "连接名称")
    private String name;

    /**
     * 连接驱动
     */
    @Schema(description = "连接驱动")
    private String dbType;

    /**
     * 主机名称
     */
    @Schema(description = "主机名称")
    private String host;

    /**
     * 端口
     */
    @Schema(description = "端口")
    private String port;
}

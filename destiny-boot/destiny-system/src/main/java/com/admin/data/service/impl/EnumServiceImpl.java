package com.admin.data.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.constant.SymbolConstant;
import com.admin.data.entity.EnumEntity;
import com.admin.data.mapper.EnumMapper;
import com.admin.data.service.EnumService;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.enums.system.FieldOrderEnum;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 枚举信息表;(s_sys_enum)表服务实现类
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/7/24
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class EnumServiceImpl extends ServiceImpl<EnumMapper, EnumEntity> implements EnumService {
    /**
     * 新增和修改枚举信息
     *
     * @param entity 枚举信息
     * @return 是否保存成功
     */
    @Override
    public boolean saveUpdate(EnumEntity entity) throws CustomException {
        if (checkExist(entity)) {
            throw new CustomException(ErrorCodesEnum.DB000);
        } else {
            if (StringUtils.isEmpty(entity.getId())) {
                entity.setId(GuIdUtil.nextId());
            }
            return this.saveOrUpdate(entity);
        }
    }

    /**
     * 验证编号是否存在
     *
     * @param entity 数据
     * @return 是否存在
     */
    public Boolean checkExist(EnumEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(EnumEntity::getCode, entity.getCode());
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            queryWrapper.ne(EnumEntity::getId, entity.getId());
            List<EnumEntity> findList = this.list(queryWrapper);
            if (!findList.isEmpty()) {
                return Boolean.TRUE;
            }
        } else {
            List<EnumEntity> findList = this.list(queryWrapper);
            if (!findList.isEmpty()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 根据查询参数查询枚举列表
     *
     * @param pageModel 查询参数
     * @return 枚举列表
     */
    @Override
    public List<EnumEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(EnumEntity::getCode, pageModel.getSearchValue()).or(
                    i -> {
                        i.like(EnumEntity::getName, pageModel.getSearchValue());
                    }
            );
        }
        if (StrUtil.isEmpty(pageModel.getSortField())) {
            queryWrapper.orderBy(EnumEntity::getCreateTime).desc();
        } else {
            queryWrapper = FieldOrderEnum.ASC.getKey().equalsIgnoreCase(pageModel.getSortOrder()) ? queryWrapper.orderBy(pageModel.getSortField(), Boolean.TRUE) : queryWrapper.orderBy(pageModel.getSortField(), Boolean.FALSE);
        }
        Page<EnumEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<EnumEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 删除枚举信息操作
     *
     * @param ids 枚举的ID
     * @return 反馈是否成功
     */
    @Override
    public boolean delete(IdsModel ids) {
        List<EnumEntity> deleteList = this.listByIds(ids.getIds());
        return this.removeByIds(deleteList.stream().map(EnumEntity::getId).toList());
    }

    /**
     * 根据ID查询单条信息
     *
     * @param id 枚举的ID
     * @return 返回信息
     */
    @Override
    public EnumEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 根据code取到枚举信息集合
     *
     * @param codes 枚举code逗号分隔
     * @return 枚举信息集合
     */
    @Override
    public List<EnumEntity> getEnumByCode(String codes) {
        QueryWrapper queryWrapper = new QueryWrapper();
        List<String> listCodes = Arrays.asList(codes.split(SymbolConstant.STRING_COMMA));
        queryWrapper.in(EnumEntity::getCode, listCodes);
        return this.list(queryWrapper);
    }
}

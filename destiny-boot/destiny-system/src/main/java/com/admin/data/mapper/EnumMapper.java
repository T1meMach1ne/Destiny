package com.admin.data.mapper;

import com.admin.data.entity.EnumEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/7/24
 */
@Mapper
public interface EnumMapper extends BaseMapper<EnumEntity> {
}

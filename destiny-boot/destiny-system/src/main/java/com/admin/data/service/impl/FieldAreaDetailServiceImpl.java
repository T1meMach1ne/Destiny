package com.admin.data.service.impl;

import com.admin.constant.DbNameConst;
import com.admin.constant.SymbolConstant;
import com.admin.data.entity.FieldAreaDetailEntity;
import com.admin.data.mapper.FieldAreaDetailMapper;
import com.admin.data.service.FieldAreaDetailService;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * 数据域主表;(s_d_field_area_detail)表服务实现类
 *
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class FieldAreaDetailServiceImpl extends ServiceImpl<FieldAreaDetailMapper, FieldAreaDetailEntity> implements FieldAreaDetailService {
    /**
     * 根据主表id获取明细
     *
     * @param ids 主表id
     * @return 子表信息
     */
    @Override
    public List<FieldAreaDetailEntity> getDetailList(List<String> ids) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(FieldAreaDetailEntity::getFieldId, ids);
        return this.list(queryWrapper);
    }

    /**
     * 根据id获取明细
     *
     * @param ids 明细表id
     * @return 子表信息
     */
    @Override
    public List<FieldAreaDetailEntity> getList(String ids) {
        List<String> listIds = Arrays.asList(ids.split(SymbolConstant.STRING_COMMA));
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(FieldAreaDetailEntity::getId, listIds);
        return this.list(queryWrapper);
    }
}

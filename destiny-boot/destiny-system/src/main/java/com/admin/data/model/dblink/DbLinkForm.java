package com.admin.data.model.dblink;

import com.admin.model.BaseEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 数据库基础表单对象
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DbLinkForm extends BaseEntity {

    /**
     * 连接名
     */
    @Schema(description = "连接名")
    @NotBlank(message = "必填")
    private String name;

    /**
     * 数据库类型编码
     */
    @Schema(description = "数据库类型编码")
    @NotBlank(message = "必填")
    private String dbType;

    /**
     * 用户
     */
    @Schema(description = "用户")
    @NotBlank(message = "必填")
    private String userName;

    /**
     * 数据库名
     */
    @Schema(description = "数据库名")
    private String serviceName;

    /**
     * 密码
     */
    @Schema(description = "密码")
    @NotBlank(message = "必填")
    private String password;

    /**
     * 端口
     */
    @Schema(description = "端口")
    @NotBlank(message = "必填")
    private String port;

    /**
     * ip地址
     */
    @Schema(description = "ip地址")
    @NotBlank(message = "必填")
    private String host;

    /**
     * 模式
     */
    @Schema(description = "模式")
    private String dbSchema;

    /**
     * 表空间
     */
    @Schema(description = "表空间")
    private String tableSpace;

    /**
     * oracle扩展
     */
    @Schema(description = "oracle扩展（true:开启，false:关闭）")
    private Boolean oracleExtend;

    /**
     * oracle连接类型
     */
    @Schema(description = "oracle连接类型")
    private String oracleLinkType;

    /**
     * oracle服务名
     */
    @Schema(description = "oracle服务名")
    private String oracleService;

    /**
     * oracle角色
     */
    @Schema(description = "oracle角色")
    private String oracleRole;
}

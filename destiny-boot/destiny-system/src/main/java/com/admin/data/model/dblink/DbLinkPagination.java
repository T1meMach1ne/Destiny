package com.admin.data.model.dblink;

import com.admin.model.PageModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/14
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class DbLinkPagination extends PageModel {

    /**
     * 连接驱动
     */
    @Schema(description = "连接驱动")
    private String dbType;
}

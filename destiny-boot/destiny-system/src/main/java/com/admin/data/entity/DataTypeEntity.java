package com.admin.data.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 系统数据类型表
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Data
@Table("base_data_type")
@Accessors(chain = true)
@Schema(name = "系统数据类型表")
@EqualsAndHashCode(callSuper = false)
public class DataTypeEntity extends BaseEntity {

    /**
     * 名称
     */
    @Schema(description = "名称")
    @Column("name")
    private String name;

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column("code")
    private String code;

    /**
     * 类型
     */
    @Schema(description = "类型")
    @Column("type")
    private String type;

    /**
     * 状态
     */
    @Schema(description = "状态")
    @Column("state")
    private String state;

}

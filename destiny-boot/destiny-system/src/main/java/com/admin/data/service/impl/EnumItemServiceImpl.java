package com.admin.data.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.data.entity.EnumEntity;
import com.admin.data.entity.EnumItemEntity;
import com.admin.data.mapper.EnumItemMapper;
import com.admin.data.model.enums.EnumItemPage;
import com.admin.data.service.EnumItemService;
import com.admin.data.service.EnumService;
import com.admin.enums.system.EnumTypeEnum;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.enums.system.FieldOrderEnum;
import com.admin.exception.CustomException;
import com.admin.model.EnumVO;
import com.admin.model.IdsModel;
import com.admin.util.GuIdUtil;
import com.admin.util.JsonUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/7/25
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class EnumItemServiceImpl extends ServiceImpl<EnumItemMapper, EnumItemEntity> implements EnumItemService {

    private final EnumService enumService;

    /**
     * 根据查询参数查询枚举列表
     *
     * @param pageModel 查询参数
     * @return 枚举列表
     * @throws Exception 异常
     */
    @Override
    public List<EnumItemEntity> list(EnumItemPage pageModel) throws Exception {
        if (ObjectUtil.isNotEmpty(pageModel.getEnumId())) {
            EnumEntity enumEntity = enumService.info(pageModel.getEnumId());
            if (enumEntity.getType().equals(EnumTypeEnum.TABLE.getKey())) {
//                String sql = enumEntity.getDbSql();
//                UserInfo userInfo = currentUtil.getUserInfo();
//                String replaceSql = StringFuncUtil.replaceRegString(sql, BeanUtilBeanUtil.beanToMap(enumEntity), userInfo);
//                DbUtil
//                JdbcTemplate jdbcTemplate=new JdbcTemplate()
//                List<Map<String, Object>> mapList = JdbcTemplate.
//                List<EnumItemEntity> list = BeanUtil.copyToList(mapList, EnumItemEntity.class);
//                return pageModel.setList(PageUtil.getListPage(pageModel.getCurrent(), pageModel.getPageSize(), list), list.size());
                return null;
            } else {
                QueryWrapper queryWrapper = new QueryWrapper();
                if (ObjectUtil.isNotEmpty(pageModel.getEnumId())) {
                    queryWrapper.eq(EnumItemEntity::getSourceId, pageModel.getEnumId());
                }
                //排序
                if (StrUtil.isEmpty(pageModel.getSortField())) {
                    queryWrapper.orderBy(EnumItemEntity::getCreateTime).desc();
                } else {
                    queryWrapper = FieldOrderEnum.ASC.getKey().equalsIgnoreCase(pageModel.getSortOrder()) ? queryWrapper.orderBy(pageModel.getSortField(), Boolean.TRUE) : queryWrapper.orderBy(pageModel.getSortField(), Boolean.FALSE);
                }
                Page<EnumItemEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
                Page<EnumItemEntity> iPage = this.page(page, queryWrapper);
                return pageModel.setList(iPage.getRecords(), page.getTotalRow());
            }
        } else {
            return null;
        }
    }

    /**
     * 根据枚举id查询枚举项明细
     *
     * @param enumId 枚举id
     * @return 枚举列表
     */
    @Override
    public List<EnumItemEntity> allList(String enumId) {
        if (ObjectUtil.isNotEmpty(enumId)) {
            EnumEntity enumEntity = enumService.info(enumId);
            if (enumEntity.getType().equals(EnumTypeEnum.TABLE.getKey())) {
//                DynamicDataSourceContextHolder.push(enumEntity.getDbName());
//                Connection conn = ConnUtil.getConn(dataSourceUtil);
//                String sql = enumEntity.getDbSql();
//                UserInfo userInfo = currentUtil.getUserInfo();
//                String replaceSql = StringFuncUtil.replaceRegString(sql, BeanUtil.beanToMap(enumEntity), userInfo);
//                List<Map<String, Object>> mapList = JdbcUtil.queryList(new PrepareSqlDTO(conn, replaceSql));
//                return BeanUtil.copyToList(mapList, EnumItemEntity.class);
                return null;
            } else {
                QueryWrapper queryWrapper = new QueryWrapper();
                if (ObjectUtil.isNotEmpty(enumId)) {
                    queryWrapper.eq(EnumItemEntity::getSourceId, enumId);
                }
                //排序
                queryWrapper.orderBy(EnumItemEntity::getSortIndex).asc();
                return this.list(queryWrapper);
            }
        } else {
            return null;
        }
    }

    /**
     * 新增和修改枚举信息
     *
     * @param entity 枚举信息
     * @return 是否保存成功
     */
    @Override
    public boolean saveUpdate(EnumItemEntity entity) throws CustomException {
        if (checkExist(entity)) {
            throw new CustomException(ErrorCodesEnum.DB000);
        } else {
            if (StringUtils.isEmpty(entity.getId())) {
                entity.setId(GuIdUtil.nextId());
            }
            return this.saveOrUpdate(entity);
        }
    }

    /**
     * 验证编号是否存在
     *
     * @param entity 枚举子项信息
     * @return 是否存在
     */
    public Boolean checkExist(EnumItemEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(EnumItemEntity::getCode, entity.getCode());
        queryWrapper.eq(EnumItemEntity::getSourceId, entity.getSourceId());
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            queryWrapper.ne(EnumItemEntity::getId, entity.getId());
            List<EnumItemEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        } else {
            List<EnumItemEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 删除枚举信息操作
     *
     * @param ids 枚举的ID
     * @return 反馈是否成功
     */
    @Override
    public boolean delete(IdsModel ids) {
        List<EnumItemEntity> deleteList = this.listByIds(ids.getIds());
        return this.removeByIds(deleteList.stream().map(EnumItemEntity::getId).toList());
    }

    /**
     * 根据ID查询枚举项单条信息
     *
     * @param id 枚举的ID
     * @return 返回信息
     */
    @Override
    public EnumItemEntity info(String id) {
        return this.getById(id);
    }

    /**
     * @param codes 枚举编号集合
     * @return 枚举voList
     */
    @Override
    public List<EnumVO> getEnumItemList(String codes) {
        List<EnumEntity> list = enumService.getEnumByCode(codes);
        List<EnumItemEntity> enumItemEntityList = commonGetEnumItem(list);
        List<EnumVO> enumVOList = new ArrayList<>();
        for (EnumEntity enumItem : list) {
            EnumVO enumVO = new EnumVO();
            enumVO.setKey(enumItem.getCode());
            if (enumItem.getType().equals(EnumTypeEnum.TABLE.getKey())) {
                String data = JsonUtil.getObjectToString(this.allList(enumItem.getId()));
                enumVO.setValue(data);
            } else {
                List<EnumItemEntity> items = enumItemEntityList.stream().filter(c -> c.getSourceId().equals(enumItem.getId())).collect(Collectors.toList());
                enumVO.setValue(JsonUtil.getObjectToString(items));
            }
            enumVOList.add(enumVO);
        }
        return enumVOList;
    }

    private List<EnumItemEntity> commonGetEnumItem(List<EnumEntity> list) {
        QueryWrapper queryWrapper = new QueryWrapper();
        List<String> enumIdList = list.stream().map(EnumEntity::getId).toList();
        queryWrapper.in(EnumItemEntity::getSourceId, enumIdList);
        return this.list(queryWrapper);
    }
}

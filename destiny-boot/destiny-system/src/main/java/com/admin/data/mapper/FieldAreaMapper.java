package com.admin.data.mapper;

import com.admin.data.entity.FieldAreaEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/8/7
 */
@Mapper
public interface FieldAreaMapper extends BaseMapper<FieldAreaEntity> {
}

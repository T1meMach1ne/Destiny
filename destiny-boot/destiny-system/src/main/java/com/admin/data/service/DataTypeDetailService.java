package com.admin.data.service;

import com.admin.data.entity.DataTypeDetailEntity;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
public interface DataTypeDetailService extends IService<DataTypeDetailEntity> {

    /**
     * 根据主表id获取明细
     *
     * @param id 主表id
     * @return 子表信息
     */
    List<DataTypeDetailEntity> getDetailList(List<String> ids);

    /**
     * 根据id获取明细
     *
     * @param id 明细表id
     * @return 子表信息
     */
    List<DataTypeDetailEntity> getList(String id);
}

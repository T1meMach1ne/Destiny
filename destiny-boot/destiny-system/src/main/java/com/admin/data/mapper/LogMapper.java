package com.admin.data.mapper;

import com.admin.data.entity.LogEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 日志表;(s_log)表数据库访问层
 *
 * @author yh_liu
 * @version 1.0
 * @since : 2022-7-17
 */
@Mapper
public interface LogMapper extends BaseMapper<LogEntity> {
}
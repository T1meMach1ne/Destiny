package com.admin.data.mapper;

import com.admin.data.entity.TableColumnEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ThePai
 * @version 1.0
 * @since 2022/8/16
 */
@Mapper
public interface TableColumnMapper extends BaseMapper<TableColumnEntity> {
}

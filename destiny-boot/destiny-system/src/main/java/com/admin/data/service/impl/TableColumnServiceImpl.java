package com.admin.data.service.impl;

import com.admin.constant.DbNameConst;
import com.admin.data.entity.TableColumnEntity;
import com.admin.data.mapper.TableColumnMapper;
import com.admin.data.service.TableColumnService;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022-08-06
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class TableColumnServiceImpl extends ServiceImpl<TableColumnMapper, TableColumnEntity> implements TableColumnService {
    /**
     * 根据主表id获取明细
     *
     * @param ids 主表id
     * @return 子表信息
     */
    @Override
    public List<TableColumnEntity> getDetailList(List<String> ids) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(TableColumnEntity::getTableId, ids);
        return this.list(queryWrapper);
    }

    /**
     * 根据id获取明细
     *
     * @param ids 明细表id
     * @return 子表信息
     */
    @Override
    public List<TableColumnEntity> getList(List<String> ids) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.in(TableColumnEntity::getId, ids);
        return this.list(queryWrapper);
    }

}

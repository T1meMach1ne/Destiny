package com.admin.permission.model.res;

import com.admin.model.TreeNode;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hw_ren
 * @since 2022-7-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuTreeVO extends TreeNode<MenuTreeVO> {

    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    private String name;

    /**
     * 路径
     */
    @Schema(description = "路径")
    private String path;

    /**
     * 重定向到子路由
     */
    @Schema(description = "重定向到子路由")
    private String redirect;

    /**
     * 组件
     */
    @Schema(description = "组件")
    private String component;

    /**
     * 菜单、面包屑、多标签页显示的名称
     */
    @Schema(description = "菜单、面包屑、多标签页显示的名称")
    private String title;

    /**
     * 菜单图标
     */
    @Schema(description = "菜单图标")
    private String icon;

    /**
     * 隐藏路由
     */
    @Schema(description = "隐藏路由(0无效1有效)")
    private String hidden;

    /**
     * 是否默认展开
     */
    @Schema(description = "是否默认展开")
    private String expanded;

    /**
     * 菜单上展示先后顺序
     */
    @Schema(description = "菜单上展示先后顺序")
    private Double orderNo;

    /**
     * 面包屑是否显示
     */
    @Schema(description = "面包屑是否显示（默认值：false）")
    private String hiddenBreadcrumb;

    /**
     * 只展示一级节点
     */
    @Schema(description = " 只展示一级节点")
    private String single;
}

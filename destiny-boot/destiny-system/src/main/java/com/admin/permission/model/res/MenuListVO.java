package com.admin.permission.model.res;

import com.admin.permission.entity.ResEntity;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

/**
 * @author hw_ren
 * @since 2022-7-26
 */
@Data
public class MenuListVO {

    /**
     * id
     */
    @Schema(description = "id")
    private String id;

    /**
     * 父id
     */
    @Schema(description = "父id")
    private String parentId;

    /**
     * 类型1
     */
    @Schema(description = "类型1:菜单 2:资源")
    private Integer type;

    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    private String name;

    /**
     * 路径
     */
    @Schema(description = "路径")
    private String path;

    /**
     * 重定向到子路由
     */
    @Schema(description = "重定向到子路由")
    private String redirect;

    /**
     * 组件
     */
    @Schema(description = "组件")
    private String component;

    /**
     * 菜单图标
     */
    @Schema(description = "菜单图标")
    private String icon;

    /**
     * 是否默认展开
     */
    @Schema(description = "是否默认展开")
    private String expanded;

    /**
     * 菜单上展示先后顺序
     */
    @Schema(description = "菜单上展示先后顺序")
    private Double orderNo;

    /**
     * 隐藏路由
     */
    @Schema(description = "隐藏路由(0无效1有效)")
    private String hidden;

    /**
     * 面包屑是否显示
     */
    @Schema(description = "面包屑是否显示（默认值：false）")
    private String hiddenBreadcrumb;

    /**
     * 只展示一级节点
     */
    @Schema(description = " 只展示一级节点")
    private String single;

    /**
     * 权限标识
     */
    @Schema(description = "权限标识")
    private String perms;

    /**
     * 权限策略1显示2禁用
     */
    @Schema(description = "权限策略1显示2禁用")
    private Integer permsType;

    /**
     * 是否添加数据权限1是0否
     */
    @Schema(description = "是否添加数据权限1是0否")
    private Integer ruleFlag;

    /**
     * 数据权限内容
     */
    @Schema(description = "数据权限内容")
    private String ruleData;

    /**
     * 状态0无效1有效
     */
    @Schema(description = "状态(0无效1有效)")
    private Integer state;

    /**
     * 排序号
     */
    @Schema(description = "排序号")
    private Integer sortIndex;

    /**
     * 存在子节点
     */
    @Schema(description = "子节点")
    private Boolean hasChildren;

    /**
     * 子节点
     */
    @Schema(description = "子节点")
    private List<ResEntity> children;

}

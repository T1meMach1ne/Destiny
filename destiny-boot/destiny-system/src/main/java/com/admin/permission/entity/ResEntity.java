package com.admin.permission.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 资源表
 * <p>
 * TableName permission
 *
 * @author hw_ren
 * @version 1.0
 * @since 2022/7/16
 */
@Data
@Accessors(chain = true)
@Schema(name = "系统资源表")
@Table(value = "base_permission_res")
@EqualsAndHashCode(callSuper = true)
public class ResEntity extends BaseEntity implements Serializable {

    /**
     * 父id
     */
    @Schema(description = "父id")
    @Column(value = "parent_id")
    private String parentId;

    /**
     * 父节点名称
     */
    @Schema(description = "父节点名称")
    @Column(value = "parent_name")
    private String parentName;

    /**
     * 类型1
     */
    @Schema(description = "类型1:菜单 2:资源")
    @Column(value = "type")
    private Integer type;

    /**
     * 菜单名称
     */
    @Schema(description = "菜单名称")
    @Column(value = "name")
    private String name;

    /**
     * 路径
     */
    @Schema(description = "路径")
    @Column(value = "path")
    private String path;

    /**
     * 重定向到子路由
     */
    @Schema(description = "重定向到子路由")
    @Column(value = "redirect")
    private String redirect;

    /**
     * 组件
     */
    @Schema(description = "组件")
    @Column(value = "component")
    private String component;

    /**
     * 菜单标题
     */
    @Schema(description = "菜单标题")
    @Column(value = "title")
    private String title;

    /**
     * 菜单图标
     */
    @Schema(description = "菜单图标")
    @Column(value = "icon")
    private String icon;

    /**
     * 是否默认展开
     */
    @Schema(description = "是否默认展开")
    @Column(value = "expanded ")
    private String expanded;

    /**
     * 菜单上展示先后顺序
     */
    @Schema(description = "菜单上展示先后顺序")
    @Column(value = "order_no ")
    private Double orderNo;

    /**
     * 隐藏路由
     */
    @Schema(description = "隐藏路由(0无效1有效)")
    @Column(value = "hidden")
    private String hidden;

    /**
     * 面包屑是否显示
     */
    @Schema(description = "面包屑是否显示（默认值：false）")
    @Column(value = "hidden_breadcrumb")
    private String hiddenBreadcrumb;

    /**
     * 多级菜单只展示一级节点
     */
    @Schema(description = " 多级菜单只展示一级节点，请注意该配置需配置在父节点")
    @Column(value = "single")
    private String single;

    /**
     * 权限标识
     */
    @Schema(description = "权限标识")
    @Column(value = "perms")
    private String perms;

    /**
     * 权限策略1显示2禁用
     */
    @Schema(description = "权限策略1显示2禁用")
    @Column(value = "perms_type")
    private Integer permsType;

    /**
     * 是否添加数据权限1是0否
     */
    @Schema(description = "是否添加数据权限1是0否")
    @Column(value = "rule_flag")
    private Integer ruleFlag;

    /**
     * 数据权限内容
     */
    @Schema(description = "数据权限内容")
    @Column(value = "rule_data")
    private String ruleData;

    /**
     * 状态
     */
    @Schema(description = "状态(Disabled无效Enabled有效)")
    @Column(value = "state")
    private String state;

    /**
     * 子节点
     */
    @Schema(description = "子节点")
    @Column(ignore = true)
    private List<ResEntity> children = new ArrayList<>();

}
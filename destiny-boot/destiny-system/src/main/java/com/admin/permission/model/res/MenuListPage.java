package com.admin.permission.model.res;

import com.admin.model.PageModel;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hw_ren
 * @since 2022-7-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class MenuListPage extends PageModel {

    /**
     * 父节点
     */
    @Schema(description = "父节点")
    private String parentId;

}

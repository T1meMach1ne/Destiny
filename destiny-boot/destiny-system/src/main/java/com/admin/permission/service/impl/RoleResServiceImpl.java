package com.admin.permission.service.impl;

import cn.hutool.core.collection.CollectionUtil;
import com.admin.base.mapper.RolePermissionMapper;
import com.admin.constant.DbNameConst;
import com.admin.permission.entity.RoleResEntity;
import com.admin.permission.service.RoleResService;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.core.update.UpdateWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
public class RoleResServiceImpl extends ServiceImpl<RolePermissionMapper, RoleResEntity> implements RoleResService {

    /**
     * 给角色绑定资源
     *
     * @param roleId           角色id
     * @param permissionIdList 资源id集合
     * @return 是否成功
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean bindPermission(Long roleId, List<Long> permissionIdList) {
        if (CollectionUtil.isEmpty(permissionIdList)) {
            return false;
        }
        List<RoleResEntity> list = new ArrayList<>();
        permissionIdList.forEach(v -> list.add(new RoleResEntity().setRoleId(roleId).setResId(v)));

        var entity = new RoleResEntity();
        UpdateWrapper<RoleResEntity> wrapper = UpdateWrapper.of(entity);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(RoleResEntity::getRoleId, roleId);

        this.update(wrapper.toEntity(), queryWrapper);
        return this.saveBatch(list);
    }
}





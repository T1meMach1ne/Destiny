package com.admin.permission.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.enums.system.FieldOrderEnum;
import com.admin.enums.system.PermissionTypeEnum;
import com.admin.model.BaseEntity;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.permission.entity.ResEntity;
import com.admin.permission.mapper.ResMapper;
import com.admin.permission.model.res.*;
import com.admin.permission.service.ResService;
import com.admin.util.CurrentUtil;
import com.admin.util.GuIdUtil;
import com.admin.util.JsonUtil;
import com.admin.util.TreeUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
@RequiredArgsConstructor
public class ResServiceImpl extends ServiceImpl<ResMapper, ResEntity> implements ResService {

    private final CurrentUtil currentUtil;

    /**
     * 获取菜单树
     *
     * @param pageModel 查询参数
     * @return 菜单树
     */
    @Override
    public List<MenuTreeVO> getMenuTree(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.MENU.getKey());
        List<ResEntity> data = this.list(queryWrapper);
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            data = data.stream().filter(t -> StrUtil.nullToEmpty(t.getTitle()).contains(pageModel.getSearchValue())
                    || StrUtil.nullToEmpty(t.getName()).contains(pageModel.getSearchValue())).collect(Collectors.toList());
        }
        data = data.stream().sorted(Comparator.comparing(ResEntity::getOrderNo)).collect(Collectors.toList());
        List<MenuTreeVO> modeList = BeanUtil.copyToList(data, MenuTreeVO.class);
        return TreeUtil.build(modeList);
    }


    /**
     * 获取菜单列表
     *
     * @return 菜单列表
     */
    @Override
    public List<ResEntity> getMenu() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.MENU.getKey());
        return this.list(queryWrapper);
    }

    /**
     * 获取资源树
     *
     * @param pageModel 查询参数
     * @return 资源树
     */
    @Override
    public List<PermissionTreeVO> getResTree(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.PERMISSION.getKey());
        List<ResEntity> treeData = this.list(queryWrapper);
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            treeData = treeData.stream().filter(t -> StrUtil.nullToEmpty(t.getTitle()).contains(pageModel.getSearchValue())
                    || StrUtil.nullToEmpty(t.getPerms()).contains(pageModel.getSearchValue())
                    || StrUtil.nullToEmpty(t.getPath()).contains(pageModel.getSearchValue())).collect(Collectors.toList());
        }
        List<PermissionTreeVO> modeList = BeanUtil.copyToList(treeData, PermissionTreeVO.class);
        return TreeUtil.build(modeList);
    }

    /**
     * 获取资源列表
     *
     * @return 资源列表
     */
    @Override
    public List<ResEntity> getRes() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.PERMISSION.getKey());
        return this.list(queryWrapper);
    }

    /**
     * 获取当前用户资源列表
     *
     * @param userId 当前用户
     * @return 资源列表
     */
    @Override
    public List<ResEntity> getResTree(String userId) {
        String cacheData = currentUtil.getUserPermission(userId);
        if (StrUtil.isNotEmpty(cacheData)) {
            return JsonUtil.getJsonToList(cacheData, ResEntity.class);
        }
        List<ResEntity> list = this.mapper.getUserRes(userId);
        currentUtil.setUserPermission(userId, JsonUtil.getObjectToString(list));
        return list;
    }

    /**
     * 获取当前用户菜单树
     *
     * @param userId 当前用户
     * @return 菜单树
     */
    @Override
    public List<MenuTreeVO> getMenuTree(String userId) {
        String cacheData = currentUtil.getUserMenuTree(userId);
        if (StrUtil.isNotEmpty(cacheData)) {
            return JsonUtil.getJsonToList(cacheData, MenuTreeVO.class);
        }
        List<ResEntity> list = this.mapper.getUserMenu(userId);
        List<MenuTreeVO> modeList = BeanUtil.copyToList(list, MenuTreeVO.class);
        List<MenuTreeVO> tree = TreeUtil.build(modeList);
        currentUtil.setUserMenuTree(userId, JsonUtil.getObjectToString(tree));
        return tree;
    }

    /**
     * 保存
     *
     * @param entity 资源信息
     * @return 是否成功
     */
    @Override
    public Boolean saveUpdate(ResEntity entity) {
        if (ObjectUtil.isEmpty(entity.getId())) {
            entity.setId(GuIdUtil.nextId());
        }
        return this.saveOrUpdate(entity);
    }

    /**
     * 根据父节点id获取资源
     *
     * @param page 父节点id
     * @return 资源列表
     */
    @Override
    public List<ResEntity> list(MenuListPage page) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.MENU.getKey());
        queryWrapper.eq(ResEntity::getParentId, page.getParentId() == null ? 0 : page.getParentId());
        //排序
        if (StrUtil.isEmpty(page.getSortField())) {
            queryWrapper.orderBy(ResEntity::getCreateTime, Boolean.FALSE);
        } else {
            queryWrapper = FieldOrderEnum.ASC.getKey().equalsIgnoreCase(page.getSortOrder()) ?
                    queryWrapper.orderBy(page.getSortField(), Boolean.TRUE) : queryWrapper.orderBy(page.getSortField(), Boolean.FALSE);
        }
        Page<ResEntity> entityPage = new Page<>(page.getCurrent(), page.getPageSize());
        Page<ResEntity> iPage = this.page(entityPage, queryWrapper);
        return page.setList(iPage.getRecords(), page.getTotal());
    }

    /**
     * 删除节点
     *
     * @param ids 节点id
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel ids) {
        List<ResEntity> list = this.listByIds(ids.getIds());
        return this.removeByIds(list.stream().map(ResEntity::getId).toList());
    }

    /**
     * 获取菜单路由
     *
     * @return 路由信息
     */
    @Override
    public List<RouterVO> getRouters() {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ResEntity::getType, PermissionTypeEnum.MENU.getKey());
        queryWrapper.ne(ResEntity::getName, "Root");
        queryWrapper.orderBy(BaseEntity::getSortIndex, Boolean.TRUE);
        List<ResEntity> list = this.list(queryWrapper);
        List<RouterVO> list1 = new ArrayList<>();
        for (ResEntity res : list) {
            RouterVO vo = new RouterVO();
            vo.setId(res.getId());
            vo.setName(res.getName());
            vo.setPath(res.getPath());
            vo.setComponent(res.getComponent());
            vo.setRedirect(res.getRedirect());
            vo.setParentId(res.getParentId());
            vo.setMeta(new MetaVO(res.getTitle(), res.getIcon(), res.getExpanded(), res.getOrderNo(), res.getHidden(), res.getHiddenBreadcrumb(), res.getSingle()));
            list1.add(vo);
        }
        return TreeUtil.build(list1);
    }
}

package com.admin.permission.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 角色权限关系表;
 *
 * @author hw_ren
 * @version 1.0
 * @since 2022/7/11
 */
@Data
@Accessors(chain = true)
@Schema(name = "角色权限关系表")
@Table("base_permission_role_res")
@EqualsAndHashCode(callSuper = true)
public class RoleResEntity extends BaseEntity {

    /**
     * 角色id
     */
    @Schema(description = "角色id")
    @Column(value = "role_id")
    private Long roleId;

    /**
     * 权限id
     */
    @Schema(description = "权限id")
    @Column(value = "res_id")
    private Long resId;
}
package com.admin.permission.controller;

import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import com.admin.permission.entity.ResEntity;
import com.admin.permission.model.res.*;
import com.admin.permission.service.ResService;
import com.admin.util.JsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 资源控制器
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/22
 */

@Tag(description = "资源管理", name = "res")
@RestController
@RequestMapping("/api/base/permission/res")
@RequiredArgsConstructor
public class ResController {
    private final ResService resService;

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "菜单详情")
    @GetMapping("/info/{id}")
    public ResEntity info(@PathVariable String id) {
        return resService.getById(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     */
    @Operation(summary = "保存菜单")
    @PostMapping("/save")
    public Boolean save(@RequestBody ResEntity entity) {
        return resService.saveUpdate(entity);
    }

    /**
     * 删除菜单
     *
     * @param ids id逗号分隔
     * @return 是否成功
     */
    @Operation(summary = "删除菜单")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return resService.delete(ids);
    }

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "获取菜单列表")
    @GetMapping("/list")
    public PageResult<?> list(MenuListPage pageModel) {
        List<ResEntity> list = resService.list(pageModel);
        List<MenuListVO> result = JsonUtil.getJsonToList(JsonUtil.getObjectToString(list), MenuListVO.class);
        PageResult<?> pageResult = new PageResult<>(result);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 获取菜单树
     *
     * @param pageModel 分页参数
     * @return 菜单树
     */
    @Operation(summary = "获取菜单树")
    @GetMapping("/getMenuTree")
    public List<MenuTreeVO> getMenuTree(PageModel pageModel) {
        return resService.getMenuTree(pageModel);
    }

    /**
     * 获取菜单子节点树
     *
     * @param parentId 父节点id
     * @return 菜单子节点树
     */
    @Operation(summary = "获取菜单子节点树")
    @GetMapping("/getSubMenuTree")
    public List<ResEntity> getSubMenuTree(Long parentId) {
        return null;
    }

    /**
     * 获取资源树
     *
     * @param pageModel 分页参数
     * @return 资源树
     */
    @Operation(summary = "获取资源树")
    @GetMapping("/getResTree")
    public List<PermissionTreeVO> getResTree(PageModel pageModel) {
        return resService.getResTree(pageModel);
    }

    /**
     * 获取路由菜单树
     *
     * @return 路由菜单树
     */
    @Operation(summary = "获取路由菜单")
    @GetMapping("/getRouters")
    public List<RouterVO> getRouters() {
        return resService.getRouters();
    }
}

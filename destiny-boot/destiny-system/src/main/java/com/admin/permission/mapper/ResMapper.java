package com.admin.permission.mapper;

import com.admin.permission.entity.ResEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
@Mapper
public interface ResMapper extends BaseMapper<ResEntity> {

    /**
     * 根据userId获取菜单列表
     *
     * @param userId 用户id
     * @return 菜单列表
     */
    @Select(" select sp.* from s_p_res sp join s_p_role_res srp on sp.id = srp.res_id\n" +
            "where sp.type = 1 and srp.role_id in (select srp.role_id from  s_b_user_role where user_id = #{userId} )")
    List<ResEntity> getUserMenu(String userId);

    /**
     * 根据userId获取资源列表
     *
     * @param userId 用户id
     * @return 资源列表
     */
    @Select(" select sp.* from s_p_role_res srp " +
            "    left join s_p_res sp on sp.id = srp.res_id " +
            "    where sp.type = 2 and srp.role_id in(select role_id from s_b_user_role where  user_id = #{userId} )")
    List<ResEntity> getUserRes(String userId);
}





package com.admin.permission.service;

import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.permission.entity.ResEntity;
import com.admin.permission.model.res.MenuListPage;
import com.admin.permission.model.res.MenuTreeVO;
import com.admin.permission.model.res.PermissionTreeVO;
import com.admin.permission.model.res.RouterVO;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
public interface ResService extends IService<ResEntity> {
    /**
     * /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<MenuTreeVO> getMenuTree(PageModel pageModel);

    /**
     * 获取菜单列表
     *
     * @return 菜单列表
     */
    List<ResEntity> getMenu();

    /**
     * 获取资源树
     *
     * @param pageModel 查询参数
     * @return 资源树信息
     */
    List<PermissionTreeVO> getResTree(PageModel pageModel);

    /**
     * 获取资源列表
     *
     * @return 资源列表
     */
    List<ResEntity> getRes();

    /**
     * 根据userId获取资源树
     *
     * @param userId 用户Id
     * @return 资源树
     */
    List<ResEntity> getResTree(String userId);

    /**
     * 根据userId菜单树
     *
     * @param userId 用户Id
     * @return 菜单树
     */
    List<MenuTreeVO> getMenuTree(String userId);

    /**
     * 保存
     *
     * @param entity 资源信息
     * @return 是否成功
     */
    Boolean saveUpdate(ResEntity entity);

    /**
     * 根据父节点id获取资源
     *
     * @param page 父节点id
     * @return 资源列表
     */
    List<ResEntity> list(MenuListPage page);

    /**
     * 删除
     *
     * @param ids 节点id合集
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);

    /**
     * 获取菜单
     *
     * @return 菜单树信息
     */
    List<RouterVO> getRouters();
}

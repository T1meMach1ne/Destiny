package com.admin.permission.model.res;

import lombok.Data;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/8/9
 */
@Data
public class MetaVO {

    /**
     * 设置该路由在侧边栏和面包屑中展示的名字
     */
    private String title;

    /**
     * 设置该路由的图标，对应路径src/assets/icons/svg
     */
    private String icon;

    /**
     * 是否默认展开
     */
    private Boolean expanded;

    /**
     * 菜单上展示先后顺序
     */
    private Double orderNo;


    /**
     * 隐藏路由(0无效1有效)
     */
    private Boolean hidden;

    /**
     * 面包屑是否显示（默认值：false）
     */
    private Boolean hiddenBreadcrumb;

    /**
     * 多级菜单只展示一级节点
     */
    private Boolean single;

    /**
     * 菜单明细
     *
     * @param icon    图标
     * @param title   名称
     * @param orderNo 排序
     */
    public MetaVO(String title, String icon, Double orderNo) {
        this.title = title;
        this.icon = icon;
        this.expanded = Boolean.FALSE;
        this.orderNo = orderNo;
        this.hidden = Boolean.FALSE;
        this.hiddenBreadcrumb = Boolean.FALSE;
    }

    /**
     * 菜单明细
     *
     * @param icon             图标
     * @param title            名称
     * @param orderNo          排序
     * @param expanded         展开
     * @param hidden           隐藏
     * @param single           单条
     * @param hiddenBreadcrumb 隐藏面包屑
     */
    public MetaVO(String title, String icon, String expanded, Double orderNo, String hidden, String hiddenBreadcrumb, String single) {
        this.title = title;
        this.icon = icon;
        this.expanded = expanded.isEmpty();
        this.orderNo = orderNo;
        this.hidden = hidden.isEmpty();
        this.hiddenBreadcrumb = hiddenBreadcrumb.isEmpty();
        this.single = single.isEmpty();
    }
}

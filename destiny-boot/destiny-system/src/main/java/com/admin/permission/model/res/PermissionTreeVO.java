package com.admin.permission.model.res;

import com.admin.model.TreeNode;
import com.mybatisflex.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hw_ren
 * @since 2022-7-26
 */

@Data
@EqualsAndHashCode(callSuper = true)
public class PermissionTreeVO extends TreeNode<PermissionTreeVO> {

    /**
     * 菜单标题
     */
    @Schema(description = "菜单标题")
    @Column(value = "title")
    private String title;

    /**
     * 路径
     */
    @Schema(description = "路径")
    private String path;

    /**
     * 权限标识
     */
    @Schema(description = "权限标识")
    private String perms;

    /**
     * 权限策略1显示2禁用
     */
    @Schema(description = "权限策略1显示2禁用")
    private String permsType;

    /**
     * 是否添加数据权限1是0否
     */
    @Schema(description = "是否添加数据权限1是0否")
    private String ruleFlag;

    /**
     * 数据权限内容
     */
    @Schema(description = "数据权限内容")
    private String ruleData;

    /**
     * 状态
     */
    @Schema(description = "状态(0无效1有效)")
    private String state;

}

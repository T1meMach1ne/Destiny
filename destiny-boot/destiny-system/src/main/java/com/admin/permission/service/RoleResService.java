package com.admin.permission.service;

import com.admin.permission.entity.RoleResEntity;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
public interface RoleResService extends IService<RoleResEntity> {

    /**
     * 给角色绑定资源
     *
     * @param roleId           角色id
     * @param permissionIdList 资源id集合
     * @return 是否成功
     */
    Boolean bindPermission(Long roleId, List<Long> permissionIdList);
}

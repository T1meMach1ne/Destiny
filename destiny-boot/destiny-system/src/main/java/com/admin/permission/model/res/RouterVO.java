package com.admin.permission.model.res;

import com.admin.model.TreeNode;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author hw_ren
 * @since 2022-7-26
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class RouterVO extends TreeNode<RouterVO> {
    /**
     * 编号
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 路由地址
     */
    private String path;

    /**
     * 重定向地址，当设置 noRedirect 的时候该路由在面包屑导航中不可被点击
     */
    private String redirect;

    /**
     * 组件地址
     */
    private String component;

    /**
     * 路由参数：如 {"id": 1, "name": "ry"}
     */
    private String query;

    /**
     * 其他元素
     */
    private MetaVO meta;
}
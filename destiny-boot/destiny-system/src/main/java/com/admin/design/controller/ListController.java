package com.admin.design.controller;


import com.admin.design.entity.ListEntity;
import com.admin.design.services.ListService;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 列表设计 前端控制器
 *
 * @author yh_liu
 * @since 2022-09-03
 */
@Tag(name = "list", description = "列表设计")
@RestController
@RequestMapping("/api/base/design/list")
@RequiredArgsConstructor
public class ListController {

    private final ListService listService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<?> list(PageModel pageModel) {
        List<ListEntity> list = listService.list(pageModel);
        PageResult<?> pageResult = new PageResult<>(list);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询信息")
    @GetMapping("/info/{id}")
    public ListEntity info(@PathVariable String id) {
        return listService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存")
    @PostMapping("/save")
    public Boolean save(@RequestBody ListEntity entity) throws CustomException {
        return listService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return listService.delete(ids);
    }
}

package com.admin.design.mapper;

import com.admin.design.entity.FormEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 表单设计 Mapper 接口
 *
 * @author yh_liu
 * @since 2022-09-03
 */
@Mapper
public interface FormMapper extends BaseMapper<FormEntity> {
}

package com.admin.design.controller;


import com.admin.design.entity.FormEntity;
import com.admin.design.services.FormService;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.model.PageResult;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 表单设计 前端控制器
 *
 * @author yh_liu
 * @since 2022-09-03
 */
@Tag(name = "form", description = "表单设计")
@RestController
@RequestMapping("/api/base/design/form")
@RequiredArgsConstructor
public class FormController {

    private final FormService formService;

    /**
     * 查询列表分页
     *
     * @param pageModel 分页参数
     * @return 分页列表
     */
    @Operation(summary = "查询列表")
    @GetMapping("/list")
    public PageResult<?> list(PageModel pageModel) {
        List<FormEntity> list = formService.list(pageModel);
        PageResult<?> pageResult = new PageResult<>(list);
        pageResult.setTotal(pageModel.getTotal());
        return pageResult;
    }

    /**
     * 查询单条信息
     *
     * @param id 主键id
     * @return 单条信息
     */
    @Operation(summary = "查询信息")
    @GetMapping("/info/{id}")
    public FormEntity info(@PathVariable String id) {
        return formService.info(id);
    }

    /**
     * 保存单条信息
     *
     * @param entity 实体
     * @return 是否成功
     * @throws CustomException 自定义异常
     */
    @Operation(summary = "保存")
    @PostMapping("/save")
    public Boolean save(@RequestBody FormEntity entity) throws CustomException {
        return formService.saveUpdate(entity);
    }

    /**
     * 批量删除信息
     *
     * @param ids 逗号分隔id
     * @return 是否成功
     */
    @Operation(summary = "删除")
    @PostMapping("/delete")
    public Boolean delete(@RequestBody IdsModel ids) {
        return formService.delete(ids);
    }
}

package com.admin.design.mapper;

import com.admin.design.entity.ListEntity;
import com.mybatisflex.core.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 列表设计 Mapper 接口
 *
 * @author yh_liu
 * @since 2022-09-03
 */
@Mapper
public interface ListMapper extends BaseMapper<ListEntity> {
}

package com.admin.design.entity;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Table;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * 列表设计表
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/3
 */
@Data
@Table("base_design_list")
@Schema(name = "表单设计")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ListEntity extends BaseEntity {

    /**
     * 编号
     */
    @Schema(description = "编号")
    @Column(value = "code")
    private String code;

    /**
     * 名称
     */
    @Schema(description = "名称")
    @Column(value = "name")
    private String name;

    /**
     * 分类Id
     */
    @Schema(description = "分类Id")
    @Column(value = "category_id")
    private String categoryId;

    /**
     * 脚本
     */
    @Schema(description = "脚本")
    @Column(value = "script_text")
    private String scriptText;

    /**
     * 页面标签
     */
    @Schema(description = "页面标签")
    @Column(value = "html_text")
    private String htmlText;
}

package com.admin.design.services;

import com.admin.design.entity.ListEntity;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.mybatisflex.core.service.IService;

import java.util.List;

/**
 * 列表设计 服务类
 *
 * @author yh_liu
 * @since 2022-09-03
 */
public interface ListService extends IService<ListEntity> {

    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    List<ListEntity> list(PageModel pageModel);

    /**
     * 新增
     *
     * @param id 主键id
     * @return 是否成功
     */
    ListEntity info(String id);

    /**
     * 保存
     *
     * @param entity 当前信息
     * @return 是否成功
     * @throws CustomException 错误信息
     */
    boolean saveUpdate(ListEntity entity) throws CustomException;

    /**
     * 删除
     *
     * @param ids id数组
     * @return 是否成功
     */
    Boolean delete(IdsModel ids);
}

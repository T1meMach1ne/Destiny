package com.admin.design.services.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.admin.constant.DbNameConst;
import com.admin.design.entity.ListEntity;
import com.admin.design.mapper.ListMapper;
import com.admin.design.services.ListService;
import com.admin.enums.system.ErrorCodesEnum;
import com.admin.exception.CustomException;
import com.admin.model.IdsModel;
import com.admin.model.PageModel;
import com.admin.util.GuIdUtil;
import com.mybatisflex.annotation.UseDataSource;
import com.mybatisflex.core.paginate.Page;
import com.mybatisflex.core.query.QueryWrapper;
import com.mybatisflex.spring.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 列表设计 服务实现类
 *
 * @author yh_liu
 * @since 2022-09-03
 */
@Service
@UseDataSource(DbNameConst.DB_BASE)
public class ListServiceImpl extends ServiceImpl<ListMapper, ListEntity> implements ListService {
    /**
     * 查询列表
     *
     * @param pageModel 查询参数
     * @return 列表数据
     */
    @Override
    public List<ListEntity> list(PageModel pageModel) {
        QueryWrapper queryWrapper = new QueryWrapper();
        if (StrUtil.isNotEmpty(pageModel.getSearchValue())) {
            queryWrapper.like(ListEntity::getName, pageModel.getSearchValue()).or(
                    x -> {
                        x.like(ListEntity::getCode, pageModel.getSearchValue());
                    }
            );
        }
        Page<ListEntity> page = new Page<>(pageModel.getCurrent(), pageModel.getPageSize());
        Page<ListEntity> iPage = this.page(page, queryWrapper);
        return pageModel.setList(iPage.getRecords(), page.getTotalRow());
    }

    /**
     * 获取信息
     *
     * @param id 主键id
     * @return 当前数据
     */
    @Override
    public ListEntity info(String id) {
        return this.getById(id);
    }

    /**
     * 保存
     *
     * @param entity 信息
     * @return 是否成功
     */
    @Override
    public boolean saveUpdate(ListEntity entity) throws CustomException {
        if (checkExist(entity)) {
            throw new CustomException(ErrorCodesEnum.DB000);
        } else {
            if (ObjectUtil.isEmpty(entity.getId())) {
                entity.setId(GuIdUtil.nextId());
            }
            return this.saveOrUpdate(entity);
        }
    }

    /**
     * 验证编号是否存在
     *
     * @param entity 信息
     * @return 是否存在
     */
    public Boolean checkExist(ListEntity entity) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq(ListEntity::getCode, entity.getCode());
        if (ObjectUtil.isNotEmpty(entity.getId())) {
            queryWrapper.ne(ListEntity::getId, entity.getId());
            List<ListEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        } else {
            List<ListEntity> list = this.list(queryWrapper);
            if (!list.isEmpty()) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    /**
     * 删除
     *
     * @param ids id数组
     * @return 是否成功
     */
    @Override
    public Boolean delete(IdsModel ids) {
        List<ListEntity> list = this.listByIds(ids.getIds());
        return this.removeByIds(list.stream().map(ListEntity::getId).toList());
    }
}

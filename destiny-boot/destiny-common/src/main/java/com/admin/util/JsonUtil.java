package com.admin.util;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author yh_liu
 * @version V2.0
 * @since 2023-9-10
 */
@Slf4j
public class JsonUtil {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    private static final String STANDARD_FORMAT = "yyyy-MM-dd HH:mm:ss";

    static {
        //对象的所有字段全部列入
        OBJECT_MAPPER.setSerializationInclusion(JsonInclude.Include.ALWAYS);
        //取消默认转换timestamps形式
        OBJECT_MAPPER.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        //忽略空Bean转json的错误
        OBJECT_MAPPER.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        //所有的日期格式都统一为以下的样式，即yyyy-MM-dd HH:mm:ss
        OBJECT_MAPPER.setDateFormat(new SimpleDateFormat(STANDARD_FORMAT));
        //忽略 在json字符串中存在，但是在java对象中不存在对应属性的情况。防止错误
        OBJECT_MAPPER.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }


    /**
     * 功能描述：创建json数组
     *
     * @return JSON数组
     */
    public static ArrayNode createArrayNode() {
        return OBJECT_MAPPER.createArrayNode();
    }

    /**
     * 功能描述：创建json对象
     *
     * @return JSON对象
     */
    public static ObjectNode createObjectNode() {
        return OBJECT_MAPPER.createObjectNode();
    }

    /**
     * list转成JSONField
     *
     * @param lists 列表
     * @return {@link List}
     */
    @SuppressWarnings("rawtypes")
    public static List listToJsonField(List lists) {
        try {
            String jsonData = OBJECT_MAPPER.writeValueAsString(lists);
            JavaType javaType = getCollectionType(List.class, List.class);
            return OBJECT_MAPPER.readValue(jsonData, javaType);
        } catch (JsonProcessingException e) {
            log.error("Parse List to List error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把java对象转换成JSON数据
     *
     * @param object java对象
     * @return JSON数据
     */
    public static String getObjectToString(Object object) {
        try {
            return OBJECT_MAPPER.writeValueAsString(object);
        } catch (Exception e) {
            log.error("Parse object to String error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 对象转成Map
     *
     * @param object 对象
     * @return {@link Map}<{@link String}, {@link Object}>
     */
    public static Map<String, Object> entityToMap(Object object) {
        try {
            String jsonString = getObjectToString(object);
            return OBJECT_MAPPER.readValue(jsonString, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("Parse Object to Map error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * String转成Map
     *
     * @param object 对象
     * @return {@link Map}<{@link String}, {@link Object}>
     */
    public static Map<String, Object> stringToMap(String object) {
        try {
            return OBJECT_MAPPER.readValue(object, new TypeReference<>() {
            });
        } catch (JsonProcessingException e) {
            log.error("Parse String to Map error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSON数据转换成指定的java对象
     *
     * @param jsonData JSON数据
     * @param clazz    指定的java对象
     * @return 指定的java对象
     */
    public static <T> T getJsonToBean(String jsonData, Class<T> clazz) {
        try {
            return OBJECT_MAPPER.readValue(jsonData, clazz);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSON数据转换成指定的java对象
     *
     * @param dto   dto对象
     * @param clazz 指定的java对象
     * @return 指定的java对象
     */
    public static <T> T getJsonToBean(Object dto, Class<T> clazz) {
        try {
            return OBJECT_MAPPER.readValue(getObjectToString(dto), clazz);
        } catch (JsonProcessingException e) {
            log.error("Parse Object to Class<T> error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSON数据转换成JSONArray数据
     *
     * @param json json
     * @return {@link ArrayNode}
     */
    public static ObjectNode getJsonToObjectNode(String json) {
        try {
            return (ObjectNode) OBJECT_MAPPER.readTree(json);
        } catch (JsonProcessingException e) {
            log.error("Parse String to JsonNode error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSON数据转换成JSONArray数据
     *
     * @param json json
     * @return {@link ArrayNode}
     */
    public static ArrayNode getJsonToJsonArray(String json) {
        try {
            JsonNode jsonNode = OBJECT_MAPPER.readTree(json);
            if (jsonNode.isArray()) {
                return (ArrayNode) jsonNode;
            }
        } catch (Exception e) {
            log.warn("Parse json to ArrayNode error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
        return OBJECT_MAPPER.createArrayNode();
    }

    /**
     * 功能描述：把List数据转换成JSONArray数据
     *
     * @param list 列表
     * @return {@link ArrayNode}
     */
    public static <T> ArrayNode getListToJsonArray(List<T> list) {
        ArrayNode arrayNode = OBJECT_MAPPER.createArrayNode();
        for (T item : list) {
            JsonNode jsonNode = OBJECT_MAPPER.valueToTree(item);
            arrayNode.add(jsonNode);
        }
        return arrayNode;
    }

    /**
     * 功能描述：把JSON数据转换成指定的java对象列表
     *
     * @param jsonData JSON数据
     * @param clazz    指定的java对象
     * @return List<T>
     */
    public static <T> List<T> getJsonToList(String jsonData, Class<T> clazz) {
        try {
            JavaType javaType = getCollectionType(ArrayList.class, clazz);
            return OBJECT_MAPPER.readValue(jsonData, javaType);
        } catch (Exception e) {
            log.warn("Parse String to List<T> error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }


    /**
     * 功能描述：把JSON数据转换成较为复杂的List<Map<String, Object>>
     *
     * @param jsonData JSON数据
     * @return List<Map < String, Object>>
     */
    public static List<Map<String, Object>> getJsonToListMap(String jsonData) {
        try {
            JavaType javaType = getCollectionType(List.class, Map.class);
            return OBJECT_MAPPER.readValue(jsonData, javaType);
        } catch (JsonProcessingException e) {
            log.warn("Parse String to List<Map<String, Object>> error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSON数据转换成指定的java对象列表
     *
     * @param dto   dto对象
     * @param clazz 指定的java对象
     * @return List<T>
     */
    public static <T> List<T> getJsonToList(Object dto, Class<T> clazz) {
        try {
            JavaType javaType = getCollectionType(List.class, clazz);
            return OBJECT_MAPPER.readValue(getObjectToString(dto), javaType);
        } catch (JsonProcessingException e) {
            log.warn("Parse Object to List<T> error : {}", e.getMessage());
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 功能描述：把JSONArray数据转换成较为复杂的List<Map<String, Object>>
     *
     * @param jsonArray JSONArray数据
     * @return List<Map < String, Object>>
     */
    public static List<Map<String, Object>> getJsonToList(ArrayNode jsonArray) {
        if (Objects.isNull(jsonArray)) {
            return new ArrayList<>();
        }
        return OBJECT_MAPPER.convertValue(getObjectToString(jsonArray), new TypeReference<>() {
        });
    }

    public static JavaType getCollectionType(Class<?> collectionClass, Class<?>... elementClasses) {
        return OBJECT_MAPPER.getTypeFactory().constructParametricType(collectionClass, elementClasses);
    }
}

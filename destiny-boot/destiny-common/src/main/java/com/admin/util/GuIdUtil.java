package com.admin.util;

import com.github.yitter.idgen.YitIdHelper;

/**
 * id获取工具
 *
 * @author yh_liu
 * @version 1.0
 * @since 2024/1/14
 */
public class GuIdUtil {

    /**
     * 生成主键id
     *
     * @return 主键Id
     */
    public static String nextId() {
        long newId = YitIdHelper.nextId();
        return String.valueOf(newId);
    }
}

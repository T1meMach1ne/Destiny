package com.admin.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/16
 */
@Data
public class UserInfo implements Serializable {

    @Serial
    private static final long serialVersionUID = 30649287;

    /**
     * 主键id
     */
    @Schema(description = "主键id")
    private String id;

    /**
     * 账号
     */
    @Schema(description = "账号")
    private String account;

    /**
     * 姓名
     */
    @Schema(description = "姓名")
    private String userName;

    /**
     * 公司id
     */
    @Schema(description = "公司id")
    private String companyId;


    /**
     * 公司名称
     */
    @Schema(description = "公司名称")
    private String companyName;

    /**
     * 组织id
     */
    @Schema(description = "组织id")
    private String orgId;

    /**
     * 组织名称
     */
    @Schema(description = "组织名称")
    private String orgName;

    /**
     * 最后一次登录ip
     */
    @Schema(description = "最后一次登录ip")
    private String lastLoginIp;

    /**
     * 最后一次登录时间
     */
    @Schema(description = "最后一次登录时间")
    private LocalDateTime lastLoginTime;

    /**
     * 最后一次登录设备
     */
    @Schema(description = "最后一次登录设备")
    private String lastLoginPlatForm;

    /**
     * 锁定时间
     */
    @Schema(description = "锁定时间")
    private Date lockTime;

    /**
     * 密码错误次数
     */
    @Schema(description = "密码错误次数")
    private Integer errorCount;

    /**
     * 修改密码时间
     */
    @Schema(description = "修改密码时间")
    private Date updatePwdTime;

    /**
     * 用户token
     */
    @Schema(description = "用户token")
    private String token;

    /**
     * 状态
     */
    @Schema(description = "状态")
    private String state;
}

package com.admin.model;

import com.mybatisflex.annotation.Column;
import com.mybatisflex.annotation.Id;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

/**
 * 默认字段对象
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Data
public class BaseEntity {

    /**
     * 主键id
     */
    @Schema(description = "主键id")
    @Id(value = "id")
    private String id;

    /**
     * 创建时间
     */
    @Column(value = "create_time")
    @Schema(description = "创建时间")
    private LocalDateTime createTime;

    /**
     * 创建人id
     */
    @Column(value = "create_user")
    @Schema(description = "创建人id")
    private String createUser;

    /**
     * 创建人
     */
    @Column(value = "create_user_name")
    @Schema(description = "创建人")
    private String createUserName;

    /**
     * 创建人组织id
     */
    @Column(value = "create_org")
    @Schema(description = "创建人组织id")
    private String createOrg;

    /**
     * 修改时间
     */
    @Column(value = "modify_time")
    @Schema(description = "修改时间")
    private LocalDateTime modifyTime;

    /**
     * 修改人id
     */
    @Column(value = "modify_user")
    @Schema(description = "修改人id")
    private String modifyUser;

    /**
     * 修改人
     */
    @Column(value = "modify_user_name")
    @Schema(description = "修改人")
    private String modifyUserName;

    /**
     * 排序号
     */
    @Column(value = "sort_index")
    @Schema(description = "排序号")
    private Integer sortIndex;

}

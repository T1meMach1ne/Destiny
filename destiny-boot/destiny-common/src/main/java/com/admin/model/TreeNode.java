package com.admin.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 树节点，所有需要实现树节点的，都需要继承该类
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/19
 */
@Data
public class TreeNode<T> implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 主键
     */
    @Schema(description = "id")
    private String id;
    /**
     * 上级ID
     */
    @Schema(description = "上级ID")
    private String parentId;
    /**
     * 子节点列表
     */
    private List<T> children = new ArrayList<>();
}
package com.admin.exception;

import com.admin.enums.system.ErrorCodesEnum;
import com.admin.model.Result;
import com.admin.util.SpringUtils;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 统一异常处理
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/11
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 自定义异常捕捉处理
     *
     * @param ex 异常信息
     * @return 统一结果
     */
    @ExceptionHandler(value = CustomException.class)
    public Result<?> customExceptionHandler(CustomException ex) {
        return Result.fail(ex.getErrorCode().toString(), ex.getErrorMessage());
    }

    @ExceptionHandler(value = Exception.class)
    public Result<?> globalException(Exception ex) {
        if (ex != null) {
            if (SpringUtils.isDev()) {
                return Result.fail(ErrorCodesEnum.A400.getKey(), ex.getMessage());
            }
        }
        return Result.fail(ErrorCodesEnum.A400.getKey(), "未知异常");
    }
}

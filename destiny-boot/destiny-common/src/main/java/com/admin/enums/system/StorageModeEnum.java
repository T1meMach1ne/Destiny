package com.admin.enums.system;

import lombok.Getter;

import java.util.Arrays;

/**
 * 文件存储方式枚举
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/7/7
 */
@Getter
public enum StorageModeEnum {

    /**
     * minio
     */
    MINIO("minio", "minio"),

    /**
     * 本地
     */
    LOCAL("local", "本地");

    /**
     * 编号
     */
    private final String key;

    /**
     * 名称
     */
    private final String value;

    StorageModeEnum(String key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * 根据枚举value获取枚举
     *
     * @param value 名称
     * @return 枚举
     */
    public static StorageModeEnum fromDesc(String value) {
        return Arrays.stream(values()).filter(v -> v.getValue().equals(value)).findFirst().orElse(null);
    }

}

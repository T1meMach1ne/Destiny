package com.admin.enums.system;

import lombok.Getter;

import java.util.Arrays;

/**
 * 菜单类型 1:菜单 2:资源
 *
 * @author hw_ren
 * @version 1.0
 * @since 2022-7-17
 */
@Getter
public enum PermissionTypeEnum {
    /**
     * 1:菜单
     */
    MENU(1, "菜单"),

    /**
     * 2:资源
     */
    PERMISSION(2, "资源");

    /**
     * 编号
     */
    private final Integer key;

    /**
     * 描述
     */
    private final String value;

    PermissionTypeEnum(Integer key, String value) {
        this.key = key;
        this.value = value;
    }

    /**
     * 根据枚举key获取枚举
     *
     * @param key 编号
     * @return 枚举
     */
    public static PermissionTypeEnum fromCode(Integer key) {
        return Arrays.stream(values()).filter(v -> v.getKey().equals(key)).findFirst().orElse(null);
    }

    /**
     * 根据枚举value获取枚举
     *
     * @param value 名称
     * @return 枚举
     */
    public static PermissionTypeEnum fromDesc(String value) {
        return Arrays.stream(values()).filter(v -> v.getValue().equals(value)).findFirst().orElse(null);
    }

}

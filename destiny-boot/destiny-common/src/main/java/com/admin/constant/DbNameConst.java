package com.admin.constant;

/**
 * 数据库常量
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/26
 */
public class DbNameConst {

    /**
     * base数据库
     */
    public static final String DB_BASE = "base";

}

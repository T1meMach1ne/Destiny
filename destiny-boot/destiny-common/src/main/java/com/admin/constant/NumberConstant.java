package com.admin.constant;

/**
 * 字符串数字常量
 *
 * @author yh_liu
 * @version 1.0
 * @since 2023/10/20
 */
public class NumberConstant {

    /**
     * 负一,树结构根节点
     */
    public static final String NEGATIVE_ONE = "-1";

    /**
     * 零
     */
    public static final String ZERO = "0";

    /**
     * 一
     */
    public static final String ONE = "1";

    /**
     * 二
     */
    public static final String TWO = "2";
}

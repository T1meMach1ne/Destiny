package com.admin.constant;

/**
 * 字符串符号常量
 *
 * @author yh_liu
 * @version 1.0
 * @since 2023/10/20
 */
public class SymbolConstant {

    /**
     * 点符号
     */
    public static final String POINT = ".";

    /**
     * 逗号
     */
    public static final String STRING_COMMA = ",";

    /**
     * 下划线
     */
    public static final String UNDERLINE = "_";

    /**
     * 美元符号
     */
    public static final String DOLLAR = "$";

    /**
     * 空格
     */
    public static final String BLANK_SPACE = " ";

    /**
     * 空
     */
    public static final String EMPTY = "";
}

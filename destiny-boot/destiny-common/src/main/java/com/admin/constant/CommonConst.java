package com.admin.constant;

/**
 * 单词常量
 *
 * @author yh_liu
 * @version 1.0
 * @since 2022/9/26
 */
public class CommonConst {

    /**
     * JWT-Code
     */
    public static final String AUTHORIZATION = "Authorization";

    /**
     * code
     */
    public static final String CODE = "code";

    /**
     * msg
     */
    public static final String MSG = "msg";

    /**
     * data
     */
    public static final String DATA = "data";

    /**
     * HTTP
     */
    public static final String HTTP = "http";

    /**
     * HTTPS
     */
    public static final String HTTPS = "https";

    /**
     * HOST
     */
    public static final String HOST = "Host";

    /**
     * 400
     */
    public static final String FOUR_HUNDRED = "400";

    /**
     * 500
     */
    public static final String FIVE_HUNDRED = "500";

    /**
     * 系统管理员账号
     */
    public static final String ADMIN = "admin";

}

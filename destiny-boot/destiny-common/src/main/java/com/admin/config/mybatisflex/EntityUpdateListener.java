package com.admin.config.mybatisflex;

import cn.dev33.satoken.exception.SaTokenException;
import cn.hutool.core.util.ObjectUtil;
import com.admin.constant.CommonConst;
import com.admin.model.BaseEntity;
import com.admin.model.UserInfo;
import com.admin.util.CurrentUtil;
import com.admin.util.SpringUtils;
import com.mybatisflex.annotation.UpdateListener;

import java.time.LocalDateTime;

/**
 * Entity实体类全局更新数据监听器
 *
 * @author yh_liu
 * @version V2.0
 * @since 2023/11/16
 */
public class EntityUpdateListener implements UpdateListener {

    private UserInfo getDefaultUser() {
        CurrentUtil userProvider = SpringUtils.getBean(CurrentUtil.class);
        UserInfo userInfo = new UserInfo();
        try {
            userInfo = userProvider.getUserInfo();
        } catch (SaTokenException | IllegalStateException ex) {
            userInfo.setId(CommonConst.ADMIN);
            userInfo.setUserName(CommonConst.ADMIN);
            userInfo.setOrgId("");
        }
        return userInfo;
    }

    @Override
    public void onUpdate(Object entity) {
        UserInfo userInfo = getDefaultUser();
        if (ObjectUtil.isNotNull(entity) && (entity instanceof BaseEntity baseEntity)) {
            baseEntity.setModifyTime(LocalDateTime.now());
            baseEntity.setModifyUser(userInfo.getId());
            baseEntity.setModifyUserName(userInfo.getUserName());
        }
    }
}

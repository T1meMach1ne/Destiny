package com.admin.config.mybatisflex;

import cn.dev33.satoken.exception.SaTokenException;
import com.admin.constant.CommonConst;
import com.admin.model.BaseEntity;
import com.admin.model.UserInfo;
import com.admin.util.CurrentUtil;
import com.admin.util.SpringUtils;
import com.mybatisflex.annotation.InsertListener;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Entity实体类全局插入数据监听器
 *
 * @author yh_liu
 * @version V2.0
 * @since 2023/11/16
 */
public class EntityInsertListener implements InsertListener {

    private UserInfo getDefaultUser() {
        CurrentUtil userProvider = SpringUtils.getBean(CurrentUtil.class);
        UserInfo userInfo = new UserInfo();
        try {
            userInfo = userProvider.getUserInfo();
        } catch (SaTokenException | IllegalStateException ex) {
            userInfo.setId(CommonConst.ADMIN);
            userInfo.setUserName(CommonConst.ADMIN);
        }
        return userInfo;
    }

    @Override
    public void onInsert(Object entity) {
        UserInfo userInfo = getDefaultUser();
        if (Objects.nonNull(entity) && (entity instanceof BaseEntity baseEntity)) {
            LocalDateTime createTime = Objects.nonNull(baseEntity.getCreateTime())
                    ? baseEntity.getCreateTime() : LocalDateTime.now();
            baseEntity.setCreateTime(createTime);
            baseEntity.setCreateUser(userInfo.getId());
            baseEntity.setCreateUserName(userInfo.getUserName());
        }
    }
}
package com.admin.config.mybatisflex;

import com.admin.model.BaseEntity;
import com.mybatisflex.annotation.KeyType;
import com.mybatisflex.core.FlexGlobalConfig;
import com.mybatisflex.core.audit.AuditManager;
import com.mybatisflex.core.audit.ConsoleMessageCollector;
import com.mybatisflex.core.mybatis.FlexConfiguration;
import com.mybatisflex.spring.boot.ConfigurationCustomizer;
import com.mybatisflex.spring.boot.MyBatisFlexCustomizer;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.logging.stdout.StdOutImpl;
import org.springframework.context.annotation.Configuration;

/**
 * MybatisFlex配置类
 *
 * @author yh_liu
 * @version V2.0
 * @since 2023/11/16
 */
@Slf4j
@Configuration
public class MybatisFlexConfig implements ConfigurationCustomizer, MyBatisFlexCustomizer {
    @Override
    public void customize(FlexConfiguration flexConfiguration) {
        //mybatis实现的打印sql到控制台，便于调试
        flexConfiguration.setLogImpl(StdOutImpl.class);
    }

    @Override
    public void customize(FlexGlobalConfig flexGlobalConfig) {
        //统一设置数据库表主键为自定义guid
        FlexGlobalConfig.KeyConfig keyConfig = new FlexGlobalConfig.KeyConfig();
        keyConfig.setKeyType(KeyType.None);
        flexGlobalConfig.setKeyConfig(keyConfig);
        // 注册全局数据填充监听器
        flexGlobalConfig.registerInsertListener(new EntityInsertListener(), BaseEntity.class);
        flexGlobalConfig.registerUpdateListener(new EntityUpdateListener(), BaseEntity.class);

        // 开启审计功能
        AuditManager.setAuditEnable(true);
        // 设置SQL审计收集器
        AuditManager.setMessageCollector(new ConsoleMessageCollector());
    }
}
